-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 22, 2022 at 06:24 AM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_wisata`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_bank_account`
--

CREATE TABLE `tb_bank_account` (
  `id_bank_account` int(11) NOT NULL,
  `bank_name` varchar(20) NOT NULL,
  `account_name` varchar(50) NOT NULL,
  `account_number` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_bank_account`
--

INSERT INTO `tb_bank_account` (`id_bank_account`, `bank_name`, `account_name`, `account_number`) VALUES
(2, 'BCA', 'SITU DATAR', '008123123');

-- --------------------------------------------------------

--
-- Table structure for table `tb_pembelian_tiket`
--

CREATE TABLE `tb_pembelian_tiket` (
  `id_pembelian` int(11) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `id_tiket` int(11) NOT NULL,
  `jumlah_tiket` int(11) NOT NULL DEFAULT 1,
  `jumlah_tiket_awal` int(11) NOT NULL,
  `jumlah_pembayaran` double NOT NULL,
  `tanggal_pembelian` timestamp NOT NULL DEFAULT current_timestamp(),
  `tanggal_check_in` datetime DEFAULT NULL,
  `status` int(5) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_pembelian_tiket`
--

INSERT INTO `tb_pembelian_tiket` (`id_pembelian`, `id_user`, `id_tiket`, `jumlah_tiket`, `jumlah_tiket_awal`, `jumlah_pembayaran`, `tanggal_pembelian`, `tanggal_check_in`, `status`) VALUES
(8, NULL, 3, 1, 1, 30000, '2022-08-25 02:23:16', NULL, 1),
(9, 3, 3, 1, 1, 15000, '2022-08-25 03:41:22', NULL, 0),
(10, 5, 4, 1, 1, 30000, '2022-08-29 17:21:55', '2022-08-30 12:23:38', 1),
(11, 6, 4, 1, 1, 20000, '2022-10-09 02:00:50', '2022-10-09 02:17:11', 1),
(12, NULL, 5, 1, 1, 50000, '2022-10-09 02:18:28', NULL, 0),
(13, 6, 16, 1, 1, 75000, '2022-10-09 04:34:41', NULL, 0),
(14, 6, 6, 1, 1, 20000, '2022-10-10 00:20:14', NULL, 0),
(15, 6, 6, 1, 1, 20000, '2022-10-10 00:20:15', NULL, 0),
(16, 3, 6, 1, 1, 0, '2022-10-12 02:00:43', NULL, 0),
(17, 3, 6, 1, 1, 20000, '2022-10-12 02:50:21', NULL, 0),
(18, NULL, 7, 1, 1, 15000, '2022-10-12 05:38:28', NULL, 0),
(19, NULL, 9, 1, 1, 20000, '2022-10-12 05:38:54', NULL, 0),
(20, 3, 7, 1, 1, 15000, '2022-10-12 05:41:54', NULL, 0),
(21, 3, 7, 1, 1, 15000, '2022-10-12 05:43:56', NULL, 0),
(22, 3, 7, 0, 3, 45000, '2022-10-13 06:10:09', '2022-10-13 06:27:16', 1),
(23, 3, 7, 1, 4, 60000, '2022-10-13 06:40:06', NULL, 0),
(24, NULL, 11, 3, 1, 150000, '2022-10-13 06:49:10', NULL, 0),
(25, NULL, 9, 1, 1, 20000, '2022-10-13 06:54:56', NULL, 1),
(26, NULL, 7, 1, 1, 15000, '2022-10-22 04:18:50', NULL, 1),
(27, 3, 6, 2, 2, 40000, '2022-10-22 04:23:24', NULL, 0),
(28, 3, 6, 2, 2, 40000, '2022-10-22 04:24:17', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_saldo`
--

CREATE TABLE `tb_saldo` (
  `id_saldo` int(11) NOT NULL,
  `harga_saldo` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_saldo`
--

INSERT INTO `tb_saldo` (`id_saldo`, `harga_saldo`) VALUES
(1, 50000);

-- --------------------------------------------------------

--
-- Table structure for table `tb_tiket`
--

CREATE TABLE `tb_tiket` (
  `id_wahana_fasilitas` int(11) NOT NULL,
  `foto_wahana` varchar(500) NOT NULL,
  `info_wahana` varchar(300) NOT NULL,
  `harga_tiket` double NOT NULL,
  `nama_wahana` varchar(50) NOT NULL,
  `jenis_tiket` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_tiket`
--

INSERT INTO `tb_tiket` (`id_wahana_fasilitas`, `foto_wahana`, `info_wahana`, `harga_tiket`, `nama_wahana`, `jenis_tiket`) VALUES
(6, 'http://127.0.0.1:8000/storage/images/1665288097_IMG-20191008-WA0016.jpg', 'maksimal bisa dinaiki oleh 2 orang dewasa, dan 1 orang anak<br>', 20000, 'sepeda air bebek', 'wahana'),
(7, 'http://127.0.0.1:8000/storage/images/1665288071_IMG-20191008-WA0020.jpg', 'Maksimal dinaki oleh 2 orang dewasa. Disarankan untuk tidak membawa anak\r\n diatas rakit.', 15000, 'rakit bambu', 'wahana'),
(8, 'http://127.0.0.1:8000/storage/images/1665288839_IMG-20191008-WA0006.jpg', 'maksimal dinaiki oleh 2 orang dewasa dan 1 orang anak<br>', 20000, 'perahu kayu', 'wahana'),
(9, 'http://127.0.0.1:8000/storage/images/1665288978_IMG-20191008-WA0012.jpg', 'minimum umur 10 tahun, dan tidak memiliki riwayat penyakit jantung<br>', 20000, 'flying fox', 'wahana'),
(10, 'http://127.0.0.1:8000/storage/images/1665289064_IMG-20191008-WA0003.jpg', 'kapasitas maksimal 6 orang, untuk 1 jam sewa.<br>', 30000, 'saung kecil', 'wahana'),
(11, 'http://127.0.0.1:8000/storage/images/1665289143_IMG-20191008-WA0037.jpg', 'kapasitas maksimal 10 orang, untuk 1 jam sewa.<br>', 50000, 'saung besar', 'wahana'),
(12, 'http://127.0.0.1:8000/storage/images/1665289348_IMG-20191008-WA0025.jpg', 'termasuk kolam memancing, free memancing, <br>', 500000, 'Gazebo', 'wahana'),
(13, 'http://127.0.0.1:8000/storage/images/1665289452_IMG-20191008-WA0034.jpg', 'kapasitas 15 orang, sudah termasuk alat karaoke, untuk sewa 1 jam<br>', 30000, 'tempat karaoke', 'wahana'),
(14, 'http://127.0.0.1:8000/storage/images/1665289559_IMG-20191008-WA0007.jpg', 'untuk sewa 1 jam, ikan yg bisa dibawa maksimal 1 kg<br>', 25000, 'tempat memacing', 'wahana'),
(15, 'http://127.0.0.1:8000/storage/images/1665289675_IMG-20191008-WA0026.jpg', 'kapasitas maksimal 15 orang, harus DP Rp.50.000.<br>', 50000, 'booking kantin', 'wahana'),
(16, 'http://127.0.0.1:8000/storage/images/1665289777_IMG-20191008-WA0008.jpg', 'untuk 1 orang, berlaku satu hari satu malam, bawa alat camping sendiri tidak kena denda.<br>', 25000, 'camping station', 'wahana');

-- --------------------------------------------------------

--
-- Table structure for table `tb_tiket_user`
--

CREATE TABLE `tb_tiket_user` (
  `id_user` int(11) NOT NULL,
  `id_tiket` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_topup`
--

CREATE TABLE `tb_topup` (
  `id_topup` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `nominal` double NOT NULL,
  `metode_pembayaran` varchar(50) NOT NULL,
  `bukti_pembayaran` varchar(500) NOT NULL,
  `tanggal_isi_saldo` timestamp NOT NULL DEFAULT current_timestamp(),
  `tanggal_konfirmasi` timestamp NULL DEFAULT NULL,
  `status_topup` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_topup`
--

INSERT INTO `tb_topup` (`id_topup`, `id_user`, `nominal`, `metode_pembayaran`, `bukti_pembayaran`, `tanggal_isi_saldo`, `tanggal_konfirmasi`, `status_topup`) VALUES
(7, 3, 50000, '2', '', '2022-08-25 04:47:30', '2022-08-24 22:11:25', 1),
(8, 5, 100000, '2', '', '2022-08-29 16:29:05', '2022-08-29 09:29:36', 1),
(9, 6, 200000, '2', '', '2022-10-09 01:53:02', '2022-10-08 18:58:40', 1),
(10, 3, 20000, '2', 'C:\\xampp\\tmp\\php1C4F.tmp', '2022-10-12 02:39:40', NULL, 0),
(11, 3, 20000, '2', 'http://127.0.0.1:8000/storage/images/1665546367_68747470733a2f2f7777772e6469636f64696e672e636f6d2f626c6f672f77702d636f6e74656e742f75706c6f6164732f323031372f31302f6469636f64696e672d6c6f676f2d7371756172652e706e67.png', '2022-10-12 03:46:08', '2022-10-11 22:43:16', 1),
(12, 3, 40000, '2', 'http://127.0.0.1:8000/storage/images/1665553354_68747470733a2f2f7777772e6469636f64696e672e636f6d2f626c6f672f77702d636f6e74656e742f75706c6f6164732f323031372f31302f6469636f64696e672d6c6f676f2d7371756172652e706e67.png', '2022-10-12 05:42:34', '2022-10-11 22:43:10', 1),
(13, 3, 123456, '2', 'http://127.0.0.1:8000/storage/images/1665635690_68747470733a2f2f7777772e6469636f64696e672e636f6d2f626c6f672f77702d636f6e74656e742f75706c6f6164732f323031372f31302f6469636f64696e672d6c6f676f2d7371756172652e706e67.png', '2022-10-13 04:34:50', NULL, 0),
(14, 3, 200000, '2', 'http://127.0.0.1:8000/storage/images/1665641341_dicoding-logo-full.png', '2022-10-13 06:09:01', '2022-10-12 23:09:26', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `id_user` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `kategori` varchar(20) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `no_telepon` varchar(20) NOT NULL,
  `saldo` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id_user`, `email`, `password`, `kategori`, `nama`, `no_telepon`, `saldo`) VALUES
(1, 'user@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 'Pengelola', 'User 1', '0812345678', 0),
(3, 'wisatawan@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 'Wisatawan', 'Wisatawan 1', '081234567844', 60000),
(4, 'pengelola@email.com', '202cb962ac59075b964b07152d234b70', 'Pengelola', 'Pengelola', '08123123123', 0),
(5, 'w2@email.com', '81dc9bdb52d04dc20036dbd8313ed055', 'Wisatawan', 'Wisatawanlain', '08123123123', 40000),
(6, 'syadiah@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'Wisatawan', 'laras', '089647941357', 65000);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_bank_account`
--
ALTER TABLE `tb_bank_account`
  ADD PRIMARY KEY (`id_bank_account`);

--
-- Indexes for table `tb_pembelian_tiket`
--
ALTER TABLE `tb_pembelian_tiket`
  ADD PRIMARY KEY (`id_pembelian`);

--
-- Indexes for table `tb_saldo`
--
ALTER TABLE `tb_saldo`
  ADD PRIMARY KEY (`id_saldo`);

--
-- Indexes for table `tb_tiket`
--
ALTER TABLE `tb_tiket`
  ADD PRIMARY KEY (`id_wahana_fasilitas`);

--
-- Indexes for table `tb_topup`
--
ALTER TABLE `tb_topup`
  ADD PRIMARY KEY (`id_topup`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id_user`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_bank_account`
--
ALTER TABLE `tb_bank_account`
  MODIFY `id_bank_account` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_pembelian_tiket`
--
ALTER TABLE `tb_pembelian_tiket`
  MODIFY `id_pembelian` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `tb_saldo`
--
ALTER TABLE `tb_saldo`
  MODIFY `id_saldo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_tiket`
--
ALTER TABLE `tb_tiket`
  MODIFY `id_wahana_fasilitas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `tb_topup`
--
ALTER TABLE `tb_topup`
  MODIFY `id_topup` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
