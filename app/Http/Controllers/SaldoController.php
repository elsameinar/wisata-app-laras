<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Http\Controllers\UserController;
use Illuminate\Support\Carbon;

class SaldoController extends Controller
{
    public function __construct()
    {
        // $this->middleware(function ($request, $next) {
        //     if ($request->session()->has('id_user')) {
        //         UserController::getUser($request, $request->session()->get('id_user'));
        //         return $next($request);
        //     } else {
        //         return redirect('login');
        //     }
        // });
    }

    public function listNominalSaldo(Request $request)
    {
        $data = DB::table('tb_saldo')->get();
        return response()->json(['status' => 200, 'message' => "Retrieve Data Successfully", 'data' => $data], 200);
    }
    public function tambahNominalSaldo(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'harga_saldo' => 'required|int'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 200);
        }

        $saldo = DB::table('tb_saldo')->insert($validator->validated());

        if ($saldo) {
            return response()->json([
                'status' => 201,
                'message' => 'Nominal Saldo successfully inserted.'
            ], 201);
        } else {
            return response()->json([
                'status' => 500,
                'message' => 'Insert failed.'
            ], 200);
        }
    }
    public function editNominalSaldo(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id_saldo' => 'required|int',
            'harga_saldo' => 'int'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 200);
        }

        $saldo = DB::table('tb_saldo')->where(array('id_saldo' => $request->id_saldo))->update(array('harga_saldo' => $request->harga_saldo));

        if ($saldo) {
            return response()->json([
                'status' => 201,
                'message' => 'Nominal Saldo successfully updated.'
            ], 201);
        } else {
            return response()->json([
                'status' => 500,
                'message' => 'Update failed.'
            ], 200);
        }
    }

    public function deleteNominalSaldo(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'id_saldo' => 'required|int'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 200);
        }

        $saldo = DB::table('tb_saldo')->where(array('id_saldo' => $request->id_saldo))->delete();

        if ($saldo) {
            return response()->json([
                'status' => 201,
                'message' => 'saldo successfully deleted'
            ], 201);
        } else {
            return response()->json([
                'status' => 500,
                'message' => 'Delete failed'
            ], 200);
        }
    }
    public function listTransferAccount(Request $request)
    {
        $data = DB::table('tb_bank_account')->get();
        return response()->json(['status' => 200, 'message' => "Retrieve Data Successfully", 'data' => $data], 200);
    }

    public function tambahTransferAccount(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'bank_name' => 'required|string|between:2,20',
            'account_name' => 'required|string|between:2,50',
            'account_number' => 'required|string|between:2,30',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 200);
        }

        $saldo = DB::table('tb_bank_account')->insert(array_merge(
            $validator->validated()
        ));

        if ($saldo) {
            return response()->json([
                'status' => 201,
                'message' => 'Transfer account successfully inserted.'
            ], 201);
        } else {
            return response()->json([
                'status' => 500,
                'message' => 'Insert failed.'
            ], 200);
        }
    }

    public function editTransferAccount(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id_bank_account' => 'required|int',
            'bank_name' => 'string|between:2,20',
            'account_name' => 'string|between:2,50',
            'account_number' => 'string|between:2,30',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 200);
        }

        unset($validator->validated()['id_bank_account']);
        $saldo = DB::table('tb_bank_account')->where(array('id_bank_account' => $request->id_bank_account))->update(array_merge(
            $validator->validated()
        ));

        if ($saldo) {
            return response()->json([
                'status' => 201,
                'message' => 'Transfer account successfully updated.'
            ], 201);
        } else {
            return response()->json([
                'status' => 500,
                'message' => 'Update failed.'
            ], 200);
        }
    }

    public function deleteTransferAccount(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'id_bank_account' => 'required|int'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 200);
        }

        $bank_account = DB::table('tb_bank_account')->where(array('id_bank_account' => $request->id_bank_account))->delete();

        if ($bank_account) {
            return response()->json([
                'status' => 201,
                'message' => 'bank_account successfully deleted'
            ], 201);
        } else {
            return response()->json([
                'status' => 500,
                'message' => 'Delete failed'
            ], 200);
        }
    }

    public function riwayatTopUp(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id_user' => 'int'
        ]);

        if ($request->id_user) {
            $data = DB::table('tb_topup')->join('tb_user', 'tb_topup.id_user', '=', 'tb_user.id_user')->join('tb_bank_account', 'tb_topup.metode_pembayaran', '=', 'tb_bank_account.id_bank_account')->where(array('tb_topup.id_user' => $request->id_user))->orderBy('tanggal_isi_saldo')->get();
        } else {
            $data = DB::table('tb_topup')->join('tb_user', 'tb_topup.id_user', '=', 'tb_user.id_user')->join('tb_bank_account', 'tb_topup.metode_pembayaran', '=', 'tb_bank_account.id_bank_account')->orderBy('tanggal_isi_saldo')->get();
        }

        return response()->json(['status' => 200, 'message' => "Retrieve Data Successfully", 'data' => $data], 200);
    }

    public function topUpSaldo(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id_user' => 'required|int',
            'nominal' => 'required|int',
            'metode_pembayaran' => 'required|string',
            'bukti_pembayaran' => 'required|mimes:png,jpg,jpeg|max:2048',
            'status_topup' => 'required|int'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 200);
        }
        if ($request->file()) {
            $fileName = time() . '_' . $request->bukti_pembayaran->getClientOriginalName();
            $filePath = $request->file('bukti_pembayaran')->storeAs('images', $fileName, 'public');
            $fileUrl = $request->getSchemeAndHttpHost() . '/storage/' . $filePath;
            
            $topup = DB::table('tb_topup')->insert(array_merge(
                $validator->validated(),
                ['bukti_pembayaran' => $fileUrl]
            ));

            if ($topup) {
                return response()->json([
                    'status' => 201,
                    'message' => 'Topup successfully inserted. Wait for confirmation.'
                ], 201);
            } else {
                return response()->json([
                    'status' => 500,
                    'message' => 'Topup failed'
                ], 201);
            }
        } else {
            return response()->json([
                'status' => 500,
                'message' => 'Topup failed'
            ], 201);
        }
    }
    public function approveTopUp(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id_topup' => 'required|int',
            'status_topup' => 'required|int' //1 gagal atau 2 berhasil
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 200);
        }
        $topup = DB::table('tb_topup')->where(array('id_topup' => $request->id_topup))->get()->first();

        $is_updated = DB::statement("UPDATE tb_user SET saldo = saldo + " . $topup->nominal . " WHERE id_user = " . $topup->id_user);

        if ($is_updated) {
            $update_topup = DB::table('tb_topup')->where(array('id_topup' => $request->id_topup))->update(array('tanggal_konfirmasi' => now(), 'status_topup' => $request->status_topup));

            if ($update_topup) {
                return response()->json([
                    'status' => 201,
                    'message' => 'Topup successfully approved'
                ], 201);
            } else {
                return response()->json([
                    'status' => 500,
                    'message' => 'Topup not approved'
                ], 200);
            }
        } else {
            return response()->json([
                'status' => 500,
                'message' => 'Update saldo user failed'
            ], 200);
        }
    }
}
