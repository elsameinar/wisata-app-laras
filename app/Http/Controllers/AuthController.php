<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class AuthController extends Controller
{
    public function actLogin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|between:2,50',
            'password' => 'required|string|between:2,50',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 200);
        }

        $user = DB::table('tb_user')->where('email', $request->email)->where('password', md5($request->password))->first();
        if ($user != null) {
            $request->session()->put('id_user', $user->id_user);
            $request->session()->put('email', $user->email);
            $request->session()->put('kategori', $user->kategori);
            $request->session()->put('nama', $user->nama);
            $request->session()->put('no_telepon', $user->no_telepon);
            $request->session()->put('saldo', $user->saldo);

            return response()->json(['status' => 200, 'message' => "Login Success"], 200);
        } else {
            return response()->json(['status' => 401, 'message' => "Wrong username or password"], 200);
        }
    }

    public function actRegister(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|between:2,50',
            'password' => 'required|string|between:2,50',
            'kategori' => 'required|string|between:2,20',
            'nama' => 'required|string|between:2,50',
            'no_telepon' => 'required|string|between:2,20'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 200);
        }

        $user = DB::table('tb_user')->insert(array_merge(
            $validator->validated(),
            ['password' => md5($request->password)]
        ));

        if ($user) {
            return response()->json([
                'status' => 201,
                'message' => 'User successfully registered'
            ], 201);
        } else {
            return response()->json([
                'status' => 500,
                'message' => 'Register failed'
            ], 200);
        }
    }
}
