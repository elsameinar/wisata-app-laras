<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class UserController extends Controller
{
    public function __construct()
    {
        // $this->middleware(function ($request, $next) {
        //     if ($request->session()->has('id_user')) {
        //         UserController::getUser($request, $request->session()->get('id_user'));
        //         return $next($request);
        //     } else {
        //         return redirect('login');
        //     }
        // });
    }

    public function logout(Request $request)
    {
        if ($request->session()->exists('id_user')) {
            $request->session()->flush();
            return redirect('login');
        } else {
            // return response()->json(['status' => 401, 'message' => "User not logged in"], 200);
            return redirect('login');
        }
    }

    // public function tambahUsers(Request $request)
    // {
    //     $validator = Validator::make($request->all(), [
    //         'email' => 'required|mimes:png,jpg,jpeg|max:2048',
    //         'password' => 'required|string|between:2,100',
    //         '' => 'required|int',
    //         'nama_wahana' => 'required|string|between:2,20',
    //         'jenis_tiket' => 'required|string|between:2,20'
    //     ]);

    //     if ($validator->fails()) {
    //         return response()->json($validator->errors()->toJson(), 200);
    //     }
    //     if ($request->file()) {
    //         $fileName = time() . '_' . $request->foto_wahana->getClientOriginalName();
    //         $filePath = $request->file('foto_wahana')->storeAs('images', $fileName, 'public');
    //         $fileUrl = $request->getSchemeAndHttpHost() . '/storage/' . $filePath;

    //         $tiket = DB::table('tb_tiket')->insert(array_merge(
    //             $validator->validated(),
    //             ['foto_wahana' => $fileUrl]
    //         ));

    //         if ($tiket) {
    //             return response()->json(['status' => 200, 'message' => "Successfuly Insert Ticket", 'file_name' => $fileName, 'file_path' => $fileUrl], 200);
    //         } else {
    //             return response()->json(['status' => 500, 'message' => "Successfuly Insert Ticket"], 200);
    //         }
    //     }
    // }

    public function editUser(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'id_user' => 'required|int',
            'email' => 'string|between:2,50',
            'kategori' => 'string|between:2,20',
            'nama' => 'string|between:2,50',
            'no_telepon' => 'string|between:2,20'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 200);
        }
        unset($validator->validated()['id_user']);
        $user = DB::table('tb_user')->where(array('id_user' => $request->id_user))->update(array_merge(
            $validator->validated()
        ));

        if ($user) {
            return response()->json([
                'status' => 201,
                'message' => 'User successfully updated'
            ], 201);
        } else {
            return response()->json([
                'status' => 500,
                'message' => 'Update failed'
            ], 200);
        }
    }

    public function deleteUser(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'id_user' => 'required|int'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 200);
        }

        $user = DB::table('tb_user')->where(array('id_user' => $request->id_user))->delete();

        if ($user) {
            return response()->json([
                'status' => 201,
                'message' => 'User successfully delete'
            ], 201);
        } else {
            return response()->json([
                'status' => 500,
                'message' => 'Delete failed'
            ], 200);
        }
    }

    public function changePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id_user_pass' => 'required|int',
            'password' => 'required|string|between:2,50',
            'new_password' => 'required|string|between:2,50'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 200);
        }

        $user = DB::table('tb_user')->where('id_user', $request->id_user_pass)->where('password', md5($request->password))->first();
        if ($user != null) {
            $is_updated = DB::statement("UPDATE tb_user SET password = '" . md5($request->new_password) . "' WHERE id_user = " . $request->id_user_pass);

            if ($is_updated) {
                return response()->json(['status' => 201, 'message' => "Update success"], 200);
            } else {
                return response()->json(['status' => 401, 'message' => "Update failed"], 200);
            }
        } else {
            return response()->json(['status' => 401, 'message' => "User not found"], 200);
        }
    }

    public static function getUser(Request $request)
    {
        $data = DB::table('tb_user')->where('id_user',$request->session()->get('id_user'))->first();
        return response()->json(['status' => 200, 'message' => "Retrieve Data Successfuly", 'data' => $data], 200);
    }

    public static function getAllUser(Request $request)
    {
        $data = DB::table('tb_user')->where([['id_user',"!=",$request->session()->get('id_user')]])->get();
        return response()->json(['status' => 200, 'message' => "Retrieve Data Successfuly", 'data' => $data], 200);
    }
    // get Data User untuk ambil data saldonya tiap masuk fungsi __construct
    // get List Data User join dengan saldo
}
