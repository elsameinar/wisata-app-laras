<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use Illuminate\Support\Carbon;

class ViewController extends Controller
{

    public function dashboard(Request $request)
    {
        if (!$request->session()->exists('id_user')) {
            return redirect('login');
        }
        $data = array(
            'user_login' => DB::table('tb_user')->where(array('id_user' => $request->session()->get('id_user')))->get()->first(),
        );
        if ($request->session()->get('kategori') == "Pengelola") {
            $data['transaksi'] = DB::table('tb_pembelian_tiket')->count();
            $data['topup'] = DB::table('tb_topup')->count();
            $data['pengguna'] = DB::table('tb_user')->where(array('kategori' => 'Wisatawan'))->count();
            $data['pengelola'] = DB::table('tb_user')->where(array('kategori' => 'Pengelola'))->count();
            $data['tiket'] = DB::table('tb_tiket')->count();
            $data['bank'] = DB::table('tb_bank_account')->count();
        } else {
            $data['transaksi'] = DB::table('tb_pembelian_tiket')->where(array('id_user' => $request->session()->get('id_user')))->count();
            $data['topup'] = DB::table('tb_topup')->where(array('id_user' => $request->session()->get('id_user')))->count();
        }
        return view('dash/dashboard', $data);
    }

    public function userprofile(Request $request)
    {
        if (!$request->session()->exists('id_user')) {
            return redirect('login');
        }
        if (!$request->session()->has('id_user')) {
            Route('logout');
        }
        $data = array(
            'user_login' => DB::table('tb_user')->where(array('id_user' => $request->session()->get('id_user')))->get()->first()
        );
        return view('dash/userprofil', $data);
    }

    public function tiket(Request $request)
    {
        if (!$request->session()->exists('id_user')) {
            return redirect('login');
        }
        $data = array(
            'user_login' => DB::table('tb_user')->where(array('id_user' => $request->session()->get('id_user')))->get()->first()
        );
        return view('dash/tiket', $data);
    }

    public function riwayat(Request $request)
    {
        if (!$request->session()->exists('id_user')) {
            return redirect('login');
        }
        $data = array(
            'user_login' => DB::table('tb_user')->where(array('id_user' => $request->session()->get('id_user')))->get()->first()
        );
        return view('dash/riwayattransaksi', $data);
    }

    public function topup(Request $request)
    {
        if (!$request->session()->exists('id_user')) {
            return redirect('login');
        }
        $data = array(
            'user_login' => DB::table('tb_user')->where(array('id_user' => $request->session()->get('id_user')))->get()->first()
        );
        return view('dash/topup', $data);
    }

    public function infotiket(Request $request)
    {
        if (!$request->session()->exists('id_user')) {
            return redirect('login');
        }
        $data = array(
            'user_login' => DB::table('tb_user')->where(array('id_user' => $request->session()->get('id_user')))->get()->first()
        );
        return view('dash/pengelola/infotiket', $data);
    }

    public function manualtiket(Request $request)
    {
        if (!$request->session()->exists('id_user')) {
            return redirect('login');
        }
        $data = array(
            'user_login' => DB::table('tb_user')->where(array('id_user' => $request->session()->get('id_user')))->get()->first()
        );
        return view('dash/pengelola/manualtiket', $data);
    }

    public function reqtopup(Request $request)
    {
        if (!$request->session()->exists('id_user')) {
            return redirect('login');
        }
        $data = array(
            'user_login' => DB::table('tb_user')->where(array('id_user' => $request->session()->get('id_user')))->get()->first()
        );
        return view('dash/pengelola/reqtopup', $data);
    }

    public function bank(Request $request)
    {
        if (!$request->session()->exists('id_user')) {
            return redirect('login');
        }
        $data = array(
            'user_login' => DB::table('tb_user')->where(array('id_user' => $request->session()->get('id_user')))->get()->first()
        );
        return view('dash/pengelola/bank', $data);
    }

    public function users(Request $request)
    {
        if (!$request->session()->exists('id_user')) {
            return redirect('login');
        }
        $data = array(
            'user_login' => DB::table('tb_user')->where(array('id_user' => $request->session()->get('id_user')))->get()->first()
        );
        return view('dash/pengelola/users', $data);
    }

    public function scan(Request $request)
    {
        if (!$request->session()->exists('id_user')) {
            return redirect('login');
        }
        $data = array(
            'user_login' => DB::table('tb_user')->where(array('id_user' => $request->session()->get('id_user')))->get()->first()
        );
        return view('dash/pengelola/scan', $data);
    }
}
