<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Http\Controllers\UserController;

class TiketingController extends Controller
{
    public function __construct()
    {
        // $this->middleware(function($request, $next)
        // {
        //     if ($request->session()->has('id_user')) {
        //         UserController::getUser($request,$request->session()->get('id_user'));
        //         return $next($request);
        //     } else {
        //         return redirect('login');
        //     }

        // });
    }

    public function riwayatPembelianTiket(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id_user' => 'int'
        ]);

        if ($request->id_user) {
            $tiket = DB::table('tb_pembelian_tiket')->join('tb_tiket', 'tb_pembelian_tiket.id_tiket', '=', 'tb_tiket.id_wahana_fasilitas')->where(array('tb_pembelian_tiket.id_user' => $request->id_user))->orderBy('tb_pembelian_tiket.tanggal_pembelian')->get();
        } else {
            $tiket = DB::table('tb_pembelian_tiket')->join('tb_tiket', 'tb_pembelian_tiket.id_tiket', '=', 'tb_tiket.id_wahana_fasilitas')->orderBy('tb_pembelian_tiket.tanggal_pembelian')->get();
        }
        $user = DB::table('tb_user')->get();
        $data = array();
        foreach ($tiket as $t) {
            if ($t->id_user != NULL) {
                foreach ($user as $u) {
                    if ($u->id_user == $t->id_user) {
                        $d = array(
                            'id_pembelian' => $t->id_pembelian,
                            'nama' => $u->nama,
                            'nama_wahana' => $t->nama_wahana,
                            'harga_tiket' => $t->harga_tiket,
                            'jumlah_tiket' => $t->jumlah_tiket,
                            'jumlah_tiket_awal' => $t->jumlah_tiket_awal,
                            'jumlah_pembayaran' => 'Rp. ' . number_format($t->jumlah_pembayaran),
                            'tanggal_pembelian' => date('d F Y', strtotime($t->tanggal_pembelian)),
                            'status' => $t->status
                        );
                        array_push($data, $d);
                    }
                }
            } else {
                $d = array(
                    'id_pembelian' => $t->id_pembelian,
                    'nama' => 'Manual',
                    'nama_wahana' => $t->nama_wahana,
                    'harga_tiket' => $t->harga_tiket,
                    'jumlah_tiket' => $t->jumlah_tiket,
                    'jumlah_tiket_awal' => $t->jumlah_tiket_awal,
                    'jumlah_pembayaran' => 'Rp. ' . number_format($t->jumlah_pembayaran),
                    'tanggal_pembelian' => date('d F Y', strtotime($t->tanggal_pembelian)),
                    'status' => $t->status
                );
                array_push($data, $d);
            }
        }
        return response()->json(['status' => 200, 'message' => "Retrieve Data Successfuly", 'data' => $data], 200);
    }

    public function checkInTicket(Request $request)
    {
        // $validator = Validator::make($request->all(), [
        //     'id_pembelian' => 'required|int',
        //     'check_in_status' => 'required|int',
        //     'jenis_tiket' => 'required'
        //     'tanggal_checkin'
        // ]);

        // if ($validator->fails()) {
        //     return response()->json($validator->errors()->toJson(), 200);
        // }

        $tiket = DB::table('tb_pembelian_tiket')->join('tb_user', 'tb_pembelian_tiket.id_user', '=', 'tb_user.id_user')->where(array('id_pembelian' => $request->id_pembelian))->get()->first();
        if ($tiket->status == 0) {
            if ($tiket->jumlah_tiket == 1) {
                $update_topup = DB::table('tb_pembelian_tiket')->where(array('id_pembelian' => $request->id_pembelian))->update(array('status' => $request->check_in_status, 'tanggal_check_in' => now()));
                $data = array(
                    'status' => 200,
                    'message' => 'Successfuly Update Status Check In',
                    'jenis_tiket' => $request->jenis_tiket,
                    'nama_pembeli' => $tiket->nama,
                    'jumlah' => 'Rp. ' . number_format($tiket->jumlah_pembayaran),
                    'tanggal_check_in' => Date('d F Y H:i:s')
                );
            } else {
                $update_topup = DB::table('tb_pembelian_tiket')->where(array('id_pembelian' => $request->id_pembelian))->update(array('jumlah_tiket' => $tiket->jumlah_tiket - 1));
                $data = array(
                    'status' => 200,
                    'message' => 'Successfuly Update Status Check In Satu Tiket',
                    'jenis_tiket' => $request->jenis_tiket,
                    'nama_pembeli' => $tiket->nama,
                    'jumlah' => 'Rp. ' . number_format($tiket->jumlah_pembayaran/$tiket->jumlah_tiket_awal),
                    'tanggal_check_in' => Date('d F Y H:i:s')
                );
            }
            
        } else if ($tiket->status == 1) {
            $data = array(
                'status' => 500,
                'message' => 'Your Tiket has been checked before !',
                'jenis_tiket' => $request->jenis_tiket,
                'nama_pembeli' => $tiket->nama,
                'jumlah' => 'Rp.' . number_format($tiket->jumlah_pembayaran),
                'tanggal_check_in' => Date('d F Y H:i:s', strtotime($tiket->tanggal_check_in))
            );
        }
        return response()->json($data, 200);
    }

    public function beliTiket(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id_tiket' => 'required|int',
            'jumlah_tiket' => 'required|int',
            'jumlah_tiket_awal' => 'required|int',
            'jumlah_pembayaran' => 'required|int',
            'id_user' => 'int',
            'status' => 'int'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 200);
        }
        
        if ($request->id_user != null) {
            $user = DB::table('tb_user')->where(array('id_user' => $request->id_user))->get()->first();
            if ($user->saldo >= $request->jumlah_pembayaran) {
                $is_updated = DB::statement("UPDATE tb_user SET saldo = saldo - " . $request->jumlah_pembayaran . " WHERE id_user = " . $request->id_user);

                if ($is_updated) {
                    $beliTiket = DB::table('tb_pembelian_tiket')->insert($validator->validated());

                    if ($beliTiket) {
                        return response()->json([
                            'status' => 201,
                            'message' => 'Pembelian Tiket successfully inserted.'
                        ], 201);
                    } else {
                        return response()->json([
                            'status' => 500,
                            'message' => 'Pembelian Tiket failed'
                        ], 200);
                    }
                } else {
                    return response()->json([
                        'status' => 500,
                        'message' => 'Update saldo user failed'
                    ], 200);
                }
            } else {
                return response()->json([
                    'status' => 500,
                    'message' => 'Saldo tidak mencukupi'
                ], 200);
            }
        } else {
            $beliTiket = DB::table('tb_pembelian_tiket')->insert($validator->validated());

            if ($beliTiket) {
                return response()->json([
                    'status' => 201,
                    'message' => 'Pembelian Tiket successfully inserted.'
                ], 201);
            } else {
                return response()->json([
                    'status' => 500,
                    'message' => 'Pembelian Tiket failed'
                ], 200);
            }
        }
    }

    public function listTiket(Request $request)
    {
        $data = DB::table('tb_tiket')->get();
        return response()->json(['status' => 200, 'message' => "Retrieve Data Successfuly", 'data' => $data], 200);
    }

    public function tambahTiket(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'foto_wahana' => 'required|mimes:png,jpg,jpeg|max:2048',
            'info_wahana' => 'required|string|between:2,100',
            'harga_tiket' => 'required|int',
            'nama_wahana' => 'required|string|between:2,20',
            'jenis_tiket' => 'required|string|between:2,20'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 200);
        }
        if ($request->file()) {
            $fileName = time() . '_' . $request->foto_wahana->getClientOriginalName();
            $filePath = $request->file('foto_wahana')->storeAs('images', $fileName, 'public');
            $fileUrl = $request->getSchemeAndHttpHost() . '/storage/' . $filePath;

            $tiket = DB::table('tb_tiket')->insert(array_merge(
                $validator->validated(),
                ['foto_wahana' => $fileUrl]
            ));

            if ($tiket) {
                return response()->json(['status' => 200, 'message' => "Successfuly Insert Ticket", 'file_name' => $fileName, 'file_path' => $fileUrl], 200);
            } else {
                return response()->json(['status' => 500, 'message' => "Fail Insert Ticket"], 200);
            }
        }
    }

    public function editTiket(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id_wahana_fasilitas' => 'required|int',
            'foto_wahana' => 'mimes:png,jpg,jpeg|max:2048',
            'info_wahana' => 'string|between:2,100',
            'harga_tiket' => 'int',
            'nama_wahana' => 'string|between:2,20',
            'jenis_tiket' => 'string|between:2,20'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 200);
        }
        unset($validator->validated()['id_wahana_fasilitas']);
        if ($request->file()) {
            $fileName = time() . '_' . $request->foto_wahana->getClientOriginalName();
            $filePath = $request->file('foto_wahana')->storeAs('images', $fileName, 'public');
            $fileUrl = $request->getSchemeAndHttpHost() . '/storage/' . $filePath;

            $tiket = DB::table('tb_tiket')->where(array('id_wahana_fasilitas' => $request->id_wahana_fasilitas))->update(array_merge(
                $validator->validated(),
                ['foto_wahana' => $fileUrl]
            ));

            if ($tiket) {
                return response()->json(['status' => 200, 'message' => "Successfuly Update Ticket", 'file_name' => $fileName, 'file_path' => $fileUrl], 200);
            } else {
                return response()->json(['status' => 500, 'message' => "Update Ticket Failed"], 200);
            }
        } else {
            unset($validator->validated()['foto_wahana']);
            $tiket = DB::table('tb_tiket')->where(array('id_wahana_fasilitas' => $request->id_wahana_fasilitas))->update(array_merge(
                $validator->validated()
            ));

            if ($tiket) {
                return response()->json(['status' => 200, 'message' => "Successfuly Update Ticket"], 200);
            } else {
                return response()->json(['status' => 500, 'message' => "Update Ticket Failed"], 200);
            }
        }
    }

    public function deleteTiket(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'id_wahana_fasilitas' => 'required|int'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 200);
        }

        $tiket = DB::table('tb_tiket')->where(array('id_wahana_fasilitas' => $request->id_wahana_fasilitas))->delete();

        if ($tiket) {
            return response()->json([
                'status' => 201,
                'message' => 'tiket successfully deleted'
            ], 201);
        } else {
            return response()->json([
                'status' => 500,
                'message' => 'Delete failed'
            ], 200);
        }
    }
}
