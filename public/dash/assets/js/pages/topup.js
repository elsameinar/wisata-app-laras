$(function () {
    $("#page-topup").addClass("active");

    $("#floatJum").on("blur", function () {
        const value = this.value.replace(/,/g, "");
        $("#floatNom").val(value);
        this.value = parseFloat(value).toLocaleString("en-US", {
            style: "decimal",
        });
    });

    $("#floatJum").keyup(function () {
        var jum = $("#floatJum").val();
        $("#floatNom").val(jum);
    });

    $("#floatJum").keyup(function (event) {
        if (
            (event.which !== 8 && event.which !== 0 && event.which < 48) ||
            event.which > 57
        ) {
            $(this).val(function (index, value) {
                $("#floatNom").val(value.replace(/\D/g, ""));
                return value.replace(/\D/g, "");
            });
        }
    });

    get_bank();
    riwayat();

    function get_bank() {
        $.ajax({
            type: "get",
            dataType: "json",
            url: url + "/list-transfer-account",
            success: function (result) {
                var res = result.data;
                var html = "";
                for (var i = 0; i < res.length; i++) {
                    html += `<option value="${res[i].id_bank_account}">${res[i].bank_name} - ${res[i].account_number} a/n ${res[i].account_name}</option>`;
                }
                $("#floatingRek").append(html);
            },
        });
    }

    function riwayat() {
        $.ajax({
            type: "post",
            dataType: "json",
            data: { id_user: id_user, _token: token },
            url: url + "/riwayat-top-up",
            success: function (result) {
                var html = "";
                var res = result.data;
                for (var i = 0; i < res.length; i++) {
                    if (res[i].status_topup == 1) {
                        var cek =
                            '<span class="text-success fw-bold fs-6 d-block "> <i class="bi bi-check-circle fw-bold text-success"></i> Berhasil </span>';
                    } else if (res[i].status_topup == 2) {
                        var cek =
                            '<span class="text-danger fw-bold fs-6 d-block "> <i class="bi bi-x fw-bold text-danger"></i> Gagal </span>';
                    } else {
                        var cek =
                            '<span class="text-warning fw-bold fs-6 d-block "> <i class="bi bi-exclamation-circle fw-bold text-warning"></i> Sedang dilakukan verifikasi </span>';
                    }
                    html += `
                        <div class="col-md-4">
                            <div class="card">
                                <div class="card-body p-3">
                                    <h6>Transaksi Topup Saldo</h6>
                                    <span class="fs-6 d-block">Rp. ${addCommas(
                                        res[i].nominal
                                    )}</span>
                                    <span class="text-muted fs-6 d-block">${
                                        res[i].bank_name
                                    } - ${res[i].account_number}</span>
                                    ${cek}
                                </div>
                                <div class="card-footer">
                                    <span class="text-muted text-sm fw-light fs-6 d-block text-center"> <small><i class="bi bi-clock"></i> ${moment(
                                        res[i].tanggal_isi_saldo
                                    ).format("Do MMMM YYYY")} </small></span>
                                </div>
                            </div>
                        </div>
                    `;
                }
                $("#riwayat-topup").html(html);
            },
        });
    }

    $("#formTopup").submit(function (e) {
        e.preventDefault();
        $.ajax({
            type: "post",
            dataType: "json",
            processData: false,
            contentType: false,
            data: new FormData(this),
            url: url + "/top-up-saldo",
            success: function (result) {
                if (result.status == 201) {
                    Swal.fire({
                        icon: "success",
                        text: result.message,
                    }).then((results) => {
                        if (results.isConfirmed) {
                            $("#closemodal").trigger("click");
                            $(".reset").trigger("click");
                            riwayat();
                        }
                    });
                } else {
                    Swal.fire({
                        icon: "error",
                        text: result.message,
                    });
                }
            },
        });
    });

    function addCommas(nStr) {
        nStr += "";
        x = nStr.split(".");
        x1 = x[0];
        x2 = x.length > 1 ? "." + x[1] : "";
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, "$1" + "," + "$2");
        }
        return x1 + x2;
    }
});
