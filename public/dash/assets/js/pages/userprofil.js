$(function () {
    var id_user = $("#main").attr("data-user");

    loaddata();

    function loaddata() {
        $.ajax({
            type: "post",
            dataType: "json",
            data: { id_user: id_user, _token: token },
            url: url + "/get_user",
            success: function (result) {
                $("#id_user").val(result.data.id_user);
                $("#id_user_pass").val(result.data.id_user);
                $("#yourName").html(result.data.nama);
                $("#yourKategori").html(result.data.kategori);
                $("#ovw-nama").html(result.data.nama);
                $("#ovw-email").html(result.data.email);
                $("#ovw-telp").html(result.data.no_telepon);
                $("#ovw-kategori").html(result.data.kategori);
                $("#nama").val(result.data.nama);
                $("#email").val(result.data.email);
                $("#no_telepon").val(result.data.no_telepon);
                $("#kategori").val(result.data.kategori);
            },
        });
    }

    $("#renewPassword").keyup(function () {
        var newpass = $("#newPassword").val();
        var renewpass = $(this).val();
        if (newpass == renewpass) {
            $("#renewPassword").removeClass("border bg-danger");
            $("#renewPassword").addClass("border bg-success");
        } else {
            $("#renewPassword").removeClass("border bg-success");
            $("#renewPassword").addClass("border bg-danger");
        }
    });

    $("#formProfil").submit(function (e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            dataType: "JSON",
            processData: false,
            contentType: false,
            data: new FormData(this),
            url: url + "/edit_user",
            success: function (result) {
                if (result.status == 201) {
                    Swal.fire({
                        icon: "success",
                        text: result.message,
                    }).then((results) => {
                        if (results.isConfirmed) {
                            loaddata();
                        }
                    });
                } else {
                    Swal.fire({
                        icon: "error",
                        text: result.message,
                    });
                }
            },
        });
    });

    $("#formPass").submit(function (e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            dataType: "JSON",
            processData: false,
            contentType: false,
            data: new FormData(this),
            url: url + "/change_password",
            success: function (result) {
                if (result.status == 201) {
                    Swal.fire({
                        icon: "success",
                        text: result.message,
                    }).then((results) => {
                        if (results.isConfirmed) {
                            loaddata();
                        }
                    });
                } else {
                    Swal.fire({
                        icon: "error",
                        text: result.message,
                    });
                }
            },
        });
    });

    $("#formTopup").submit(function (e) {
        e.preventDefault();
        Swal.fire({
            icon: "success",
            html: "<strong> Top up sedang diproses !</strong>",
        });
    });

    $("#floatJum").on("blur", function () {
        const value = this.value.replace(/,/g, "");
        this.value = parseFloat(value).toLocaleString("en-US", {
            style: "decimal",
        });
    });

    $("#floatJum").keyup(function (event) {
        if (
            (event.which !== 8 && event.which !== 0 && event.which < 48) ||
            event.which > 57
        ) {
            $(this).val(function (index, value) {
                return value.replace(/\D/g, "");
            });
        }
    });
});
