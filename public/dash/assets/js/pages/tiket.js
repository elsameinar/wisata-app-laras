$(function () {
    $("#page-tiket").addClass("active");

    loaddata();

    $(document).on("click", ".belitiket", function () {
        var id = $(this).attr("id");
        var foto = $(this).attr("data-foto");
        var harga = $(this).attr("data-harga");
        var nama = $(this).attr("data-nama");

        $("#foto_wahana").attr("src", foto);
        $("#id_tiket").val(id);
        $("#floatHrg").val("Rp. " + addCommas(harga));
        $("#floatHrg").attr("data-hrg", harga);
        $("#floatPm").val("Rp. " + addCommas(harga));
        $("#jmlh").val(harga);
        $("#namaTiket").html(nama);

        $("#btn-tiket").trigger("click");
    });

    $("#floatJmlh").on("keyup mouseup", function () {
        var tiket = $("#floatHrg").attr("data-hrg");
        var org = $("#floatJmlh").val();
        var jmlh = org * tiket;
        $("#floatPm").val("Rp. " + addCommas(jmlh));
        $("#jmlh").val(jmlh);
        $("#jumlah_tiket_awal").val(org);
    });

    function loaddata() {
        $.ajax({
            type: "GET",
            dataType: "JSON",
            url: url + "/list-tiket",
            success: function (result) {
                var html = "";
                for (var i = 0; i < result.data.length; i++) {
                    html += `
                        <div class="col-md-3">
                            <div class="card">
                                <img src="${
                                    result.data[i].foto_wahana
                                }" class="card-img-top" alt="default.jpg">
                                <div class="card-body">
                                    <h5 class="card-title">${
                                        result.data[i].nama_wahana
                                    }</h5>
                                    <p class="card-text">${
                                        result.data[i].info_wahana
                                    }</p>
                                    <p class="card-text fw-bold">Rp. ${addCommas(
                                        result.data[i].harga_tiket
                                    )}</p>
                                    <button class="btn btn-primary btn-sm belitiket" id="${
                                        result.data[i].id_wahana_fasilitas
                                    }" data-foto="${
                        result.data[i].foto_wahana
                    }" data-harga="${result.data[i].harga_tiket}" data-nama="${
                        result.data[i].nama_wahana
                    }">Beli Tiket</a>
                                </div>
                            </div>
                        </div>
                    `;
                }
                $("#list-tiket").html(html);
            },
        });
        ``;
    }

    $("#formTiket").submit(function (e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            dataType: "JSON",
            processData: false,
            contentType: false,
            data: new FormData(this),
            url: url + "/beli-tiket",
            success: function (result) {
                if (result.status == 201) {
                    Swal.fire({
                        icon: "success",
                        text: result.message,
                        // html: '<a href="' + url + '/riwayat-pembelian-tiket">  </a>',
                    }).then((results) => {
                        if (results.isConfirmed) {
                            $("#closemodal").trigger("click");
                            $("#btn-reset").trigger("click");
                        }
                    });
                } else {
                    Swal.fire({
                        icon: "error",
                        text: result.message,
                    });
                }
            },
        });
    });

    function addCommas(nStr) {
        nStr += "";
        x = nStr.split(".");
        x1 = x[0];
        x2 = x.length > 1 ? "." + x[1] : "";
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, "$1" + "," + "$2");
        }
        return x1 + x2;
    }
});
