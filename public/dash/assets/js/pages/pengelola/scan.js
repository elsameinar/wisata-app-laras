$(function () {
    $("#page-scan").addClass("active");

    function onScanSuccess(decodedText, decodedResult) {
        // Handle on success condition with the decoded text or result.
        console.log(`Scan result: ${decodedText}`, decodedResult);
        html5QrcodeScanner.clear();
        // ^ this will stop the scanner (video feed) and clear the scan area.
        var text = `${decodedText}`;

        var split = text.split(",");
        senddata(split);
    }

    function senddata(data) {
        $.ajax({
            type: "post",
            dataType: "json",
            data: {
                id_pembelian: parseInt(data[0]),
                check_in_status: parseInt(data[1]),
                jenis_tiket: data[2],
                _token: token,
            },
            url: url + "/check-in-tiket",
            success: function (res) {
                if (res.status == 200) {
                    var html = `
                        <p> Nama Pembeli : ${res.nama_pembeli} </p>
                        <p> Jenis Tiket : ${res.jenis_tiket} </p>
                        <p> Jumlah Tiket : 1 Tiket </p>
                        <p> Jumlah Pembayaran : ${res.jumlah} </p>
                        <p> Tanggal Checkin : ${res.tanggal_check_in} </p>
                    `;
                    Swal.fire({
                        icon: "success",
                        title: res.message,
                        html: html,
                    }).then((results) => {
                        if (results.isConfirmed) {
                            html5QrcodeScanner.render(onScanSuccess);
                        }
                    });
                } else {
                    var html = `
                        <p> Nama Pembeli : ${res.nama_pembeli} </p>
                        <p> Jenis Tiket : ${res.jenis_tiket} </p>
                        <p> Jumlah Pembayaran : ${res.jumlah} </p>
                        <p> Tanggal Checkin : ${res.tanggal_check_in} </p>
                    `;
                    Swal.fire({
                        icon: "warning",
                        title: res.message,
                        html: html,
                    }).then((results) => {
                        if (results.isConfirmed) {
                            html5QrcodeScanner.render(onScanSuccess);
                        }
                    });
                }
            },
        });
    }

    var html5QrcodeScanner = new Html5QrcodeScanner("reader", {
        fps: 10,
        qrbox: 250,
    });
    html5QrcodeScanner.render(onScanSuccess);
});
