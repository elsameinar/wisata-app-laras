$(function () {
    $("#tiket-collapsed").removeClass("collapsed");
    $("#icons-nav").removeClass("collapse");
    $("#page-infotiket").addClass("active");

    $("#btn-form").click(function () {
        $(".reset").trigger("click");
        $("#floatingInfo").summernote("code", "");
    });

    $("#floatingInfo").summernote({
        placeholder: "Info Wahana",
        toolbar: [
            ["style", ["bold", "italic", "underline", "clear"]],
            ["para", ["ul", "ol", "paragraph"]],
        ],
    });

    // $("#floatHarga").on("blur", function () {
    //     const value = this.value.replace(/,/g, "");
    //     this.value = parseFloat(value).toLocaleString("en-US", {
    //         style: "decimal",
    //     });
    // });

    // $("#floatHarga").keyup(function (event) {
    //     if (
    //         (event.which !== 8 && event.which !== 0 && event.which < 48) ||
    //         event.which > 57
    //     ) {
    //         $(this).val(function (index, value) {
    //             return value.replace(/\D/g, "");
    //         });
    //     }
    // });

    $(document).on("click", ".edititem", function () {
        var id = $(this).data("id");
        var foto = $(this).data("foto");
        var info = $(this).data("info");
        var harga = $(this).data("harga");
        var nama = $(this).data("nama");
        var jenis = $(this).data("jenis");
        console.log(id, foto, info, harga, nama, jenis);
        edititem(id, foto, info, harga, nama, jenis);
    });

    $(document).on("click", ".deleteitem", function () {
        var id = $(this).data("id");
        deleteitem(id);
    });

    loaddata();

    function loaddata() {
        $.ajax({
            type: "get",
            dataType: "json",
            url: url + "/list-tiket",
            data: {
                param: "",
            },
            success: function (result) {
                var dt = $("#table-tiket").DataTable({
                    responsive: true,
                    bDestroy: true,
                    processing: false,
                    autoWidth: true,
                    pageLength: 10,
                    lengthChange: true,
                    aaData: result.data,
                    aoColumns: [
                        { mDataProp: "id_wahana_fasilitas" },
                        { mDataProp: "foto_wahana" },
                        { mDataProp: "info_wahana" },
                        { mDataProp: "harga_tiket" },
                        { mDataProp: "nama_wahana" },
                        { mDataProp: "jenis_tiket" },
                        { mDataProp: "id_wahana_fasilitas" },
                        // { 'mDataProp': 'role'},
                    ],
                    order: [[0, "ASC"]],
                    aoColumnDefs: [
                        {
                            mRender: function (data, type, row) {
                                var $rowData = "";
                                $rowData +=
                                    `
                                        <div class="dropdown">
                                            <button class="btn btn-primary btn-sm dropdown-toggle p-2" type="button" id="dropdownMenuIconButton1" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="bi bi-menu-button-wide"></i>
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton1">
                                                <a class="dropdown-item p-2 edititem" id="` +
                                    row.id +
                                    `" data-id="${row.id_wahana_fasilitas}" data-foto="${row.foto_wahana}" data-info="${row.info_wahana}" data-harga="${row.harga_tiket}" data-nama="${row.nama_wahana}" data-jenis="${row.jenis_tiket}" href="javascript:void(0)"><i class="bi bi-pen mt-4 p-2"></i> Edit</a>
                                                <a class="dropdown-item p-2 deleteitem" id="` +
                                    row.id +
                                    `" href="javascript:void(0)" data-id="${row.id_wahana_fasilitas}"><i class="bi bi-trash mt-4 p-2"></i> Hapus</a>
                                            </div>
                                        </div>
                                                    `;

                                return $rowData;
                            },
                            aTargets: [6],
                        },
                        {
                            mRender: function (data, type, row) {
                                var $rowData =
                                    "Rp. " + addCommas(row.harga_tiket);
                                return $rowData;
                            },
                            aTargets: [3],
                        },
                        {
                            mRender: function (data, type, row) {
                                var $rowData =
                                    `<img src="` +
                                    row.foto_wahana +
                                    `" class="rounded" style="max-width: 70%; alt="default.jpg">`;
                                return $rowData;
                            },
                            aTargets: [1],
                        },
                        // {
                        //     mRender: function (data, type, row){
                        //       var $rowData = '<img src="'+row.foto+'" style="width: 35px;"></img>';
                        //         return $rowData;
                        //     },
                        //     aTargets: [1]
                        // }
                    ],

                    fnRowCallback: function (
                        nRow,
                        aData,
                        iDisplayIndex,
                        iDisplayIndexFull
                    ) {
                        var index = iDisplayIndexFull + 1;
                        $("td:eq(0)", nRow).html(" " + index);
                        return;
                    },

                    fnInitComplete: function () {
                        var that = this;
                        var td;
                        var tr;

                        this.$("td").click(function () {
                            td = this;
                        });
                        this.$("tr").click(function () {
                            tr = this;
                        });

                        $("#listproj_filter input").bind("keyup", function (e) {
                            return this.value;
                        });
                    },
                });
            },
        });
    }

    $("#formTiket").submit(function (e) {
        e.preventDefault();
        var id = $("#id").val();
        var newurl = "";
        if (id) {
            newurl = url + "/edit-tiket";
        } else {
            newurl = url + "/tambah-tiket";
        }
        var newurl = $.ajax({
            type: "POST",
            dataType: "JSON",
            processData: false,
            contentType: false,
            data: new FormData(this),
            url: newurl,
            success: function (result) {
                if (result.status == 200) {
                    Swal.fire({
                        icon: "success",
                        text: result.message,
                    }).then((results) => {
                        if (results.isConfirmed) {
                            $("#closemodal").trigger("click");
                            location.reload();
                        }
                    });
                } else {
                    Swal.fire({
                        icon: "error",
                        text: result.message,
                    });
                }
            },
        });
    });

    function edititem(id, foto, info, harga, nama, jenis) {
        $("#btn-form").trigger("click");
        $("#id").val(id);
        // $("#floatingImg").val(foto);
        $("#floatingInfo").summernote("code", info);
        $("#floatHarga").val(harga);
        $("#floatNama").val(nama);
        $("#floatingJenis").val(jenis);
    }

    function deleteitem(id) {
        Swal.fire({
            title: "Yakin Di Hapus ?",
            text: "Data akan terhapus permanen !",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Ya, Hapus !",
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    type: "POST",
                    dataType: "JSON",
                    data: { id_wahana_fasilitas: id, _token: token },
                    url: url + "/delete-tiket",
                    success: function (result) {
                        if (result.status == 201) {
                            Swal.fire({
                                icon: "success",
                                text: result.message,
                            }).then((results) => {
                                if (results.isConfirmed) {
                                    loaddata();
                                }
                            });
                        } else {
                            Swal.fire({
                                icon: "error",
                                text: result.message,
                            });
                        }
                    },
                });
            }
        });
    }

    function addCommas(nStr) {
        nStr += "";
        x = nStr.split(".");
        x1 = x[0];
        x2 = x.length > 1 ? "." + x[1] : "";
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, "$1" + "," + "$2");
        }
        return x1 + x2;
    }
});
