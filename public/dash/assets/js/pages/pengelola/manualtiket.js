$(function () {
    $("#tiket-collapsed").removeClass("collapsed");
    $("#icons-nav").removeClass("collapse");
    $("#page-manualtiket").addClass("active");

    $("#floatHrg").on("blur", function () {
        const value = this.value.replace(/,/g, "");
        this.value = parseFloat(value).toLocaleString("en-US", {
            style: "decimal",
        });
    });

    gettiket();

    $("#floatTipe").change(function () {
        var selected = $(this).find("option:selected");
        var tiket = selected.data("harga");

        $("#floatHrg").val("Rp. " + addCommas(tiket));
        var org = $("#floatJmlh").val();
        var jmlh = org * tiket;
        $("#floatPm").val("Rp. " + addCommas(jmlh));
        $("#jmlh").val(jmlh);
        $("#jumlah_tiket_awal").val(org);
    });

    $("#floatJmlh").on("keyup mouseup", function () {
        var selected = $("#floatTipe").find("option:selected");
        var tiket = selected.data("harga");
        var org = $("#floatJmlh").val();
        var jmlh = org * tiket;
        $("#floatPm").val("Rp. " + addCommas(jmlh));
        $("#jmlh").val(jmlh);
    });

    function gettiket() {
        $.ajax({
            type: "get",
            dataType: "json",
            url: url + "/list-tiket",
            success: function (result) {
                var html = "";
                for (var i = 0; i < result.data.length; i++) {
                    html +=
                        '<option value="' +
                        result.data[i].id_wahana_fasilitas +
                        '" data-harga="' +
                        result.data[i].harga_tiket +
                        '">Tiket ' +
                        result.data[i].jenis_tiket +
                        " " +
                        result.data[i].nama_wahana +
                        "</option>";
                }
                $("#floatTipe").append(html);
            },
        });
    }

    $("#formTiket").submit(function (e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            dataType: "JSON",
            data: new FormData(this),
            processData: false,
            contentType: false,
            url: url + "/beli-tiket",
            success: function (result) {
                if (result.status == 201) {
                    Swal.fire({
                        icon: "success",
                        text: result.message,
                    }).then((results) => {
                        if (results.isConfirmed) {
                            $("#btn-reset").trigger("click");
                        }
                    });
                } else {
                    Swal.fire({
                        icon: "error",
                        text: result.message,
                    });
                }
            },
        });
    });

    function addCommas(nStr) {
        nStr += "";
        x = nStr.split(".");
        x1 = x[0];
        x2 = x.length > 1 ? "." + x[1] : "";
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, "$1" + "," + "$2");
        }
        return x1 + x2;
    }
});
