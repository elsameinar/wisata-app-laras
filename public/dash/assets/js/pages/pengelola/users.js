$(function () {
    $("#page-users").addClass("active");

    // $("#table-users").DataTable({
    //     dom: "Bfrtip",
    //     buttons: ["copy", "csv", "excel", "pdf", "print"],
    // });

    loaddata();

    $(document).on("click", ".deleteitem", function () {
        var id = $(this).attr("id");
        deleteitem(id);
    });

    function loaddata() {
        $.ajax({
            type: "post",
            dataType: "json",
            url: url + "/get_all_user",
            data: {
                _token: token,
            },
            success: function (result) {
                var dt = $("#table-users").DataTable({
                    responsive: true,
                    bDestroy: true,
                    processing: true,
                    autoWidth: true,
                    pageLength: 10,
                    lengthChange: true,
                    aaData: result.data,
                    dom: "Bfrtip",
                    buttons: ["copy", "csv", "excel", "pdf", "print"],
                    aoColumns: [
                        { mDataProp: "id_user" },
                        { mDataProp: "email" },
                        { mDataProp: "kategori" },
                        { mDataProp: "nama" },
                        { mDataProp: "no_telepon" },
                        { mDataProp: "no_telepon" },
                        { mDataProp: "id_user" },
                        // { 'mDataProp': 'role'},
                    ],
                    order: [[0, "ASC"]],
                    aoColumnDefs: [
                        {
                            mRender: function (data, type, row) {
                                var $rowData = "";
                                if (row.kategori == "Pengelola") {
                                    $rowData +=
                                        `
                                        <div class="dropdown">
                                            <button class="btn btn-primary btn-sm dropdown-toggle p-2" type="button" id="dropdownMenuIconButton1" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="bi bi-menu-button-wide"></i>
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton1">
                                                <a class="dropdown-item p-2 deleteitem" id="` +
                                        row.id_user +
                                        `" href="javascript:void(0)"><i class="bi bi-trash mt-4 p-2"></i> Hapus</a>
                                            </div>
                                        </div>
                                                    `;
                                }

                                return $rowData;
                            },
                            aTargets: [6],
                        },
                        {
                            mRender: function (data, type, row) {
                                var $rowData = "";
                                if (row.kategori == "Pengelola") {
                                    $rowData += "-";
                                }
                                return $rowData;
                            },
                            aTargets: [5],
                        },
                    ],

                    fnRowCallback: function (
                        nRow,
                        aData,
                        iDisplayIndex,
                        iDisplayIndexFull
                    ) {
                        var index = iDisplayIndexFull + 1;
                        $("td:eq(0)", nRow).html(" " + index);
                        return;
                    },

                    fnInitComplete: function () {
                        var that = this;
                        var td;
                        var tr;

                        this.$("td").click(function () {
                            td = this;
                        });
                        this.$("tr").click(function () {
                            tr = this;
                        });

                        $("#listproj_filter input").bind("keyup", function (e) {
                            return this.value;
                        });
                    },
                });
            },
        });
    }

    $("#formUsers").submit(function (e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            dataType: "JSON",
            data: new FormData(this),
            processData: false,
            contentType: false,
            url: url + "/act_register",
            success: function (result) {
                if (result.status == 201) {
                    Swal.fire({
                        icon: "success",
                        text: result.message,
                    }).then((results) => {
                        if (results.isConfirmed) {
                            loaddata();
                            $("#btn-reset").trigger("click");
                            $("#closemodal").trigger("click");
                        }
                    });
                } else {
                    Swal.fire({
                        icon: "error",
                        text: result.message,
                    });
                }
            },
        });
    });

    function deleteitem(id) {
        $.ajax({
            type: "POST",
            dataType: "JSON",
            data: { id_user: id_user },
            url: url,
            success: function (result) {},
        });
    }
});
