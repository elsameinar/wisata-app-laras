$(function () {
    $("#page-reqtopup").addClass("active");

    loaddata();

    $(document).on("click", ".gantistatus", function () {
        var id = $(this).attr("id");
        var status = $(this).data("status");
        gantistatus(id, status);
    });

    function loaddata() {
        $.ajax({
            type: "post",
            dataType: "json",
            url: url + "/riwayat-top-up",
            data: {
                _token: token,
            },
            success: function (result) {
                var dt = $("#table-reqtopup").DataTable({
                    responsive: true,
                    bDestroy: true,
                    processing: true,
                    autoWidth: true,
                    pageLength: 10,
                    lengthChange: true,
                    aaData: result.data,
                    dom: "Bfrtip",
                    buttons: ["copy", "csv", "excel", "pdf", "print"],
                    aoColumns: [
                        { mDataProp: "id_topup" },
                        { mDataProp: "nama" },
                        { mDataProp: "nominal" },
                        { mDataProp: "bank_name" },
                        { mDataProp: "bukti_pembayaran" },
                        { mDataProp: "tanggal_isi_saldo" },
                        { mDataProp: "tanggal_konfirmasi" },
                        { mDataProp: "status_toptup" },
                        { mDataProp: "id_user" },
                        // { 'mDataProp': 'role'},
                    ],
                    order: [[0, "ASC"]],
                    aoColumnDefs: [
                        {
                            mRender: function (data, type, row) {
                                var $rowData = "";
                                if (row.status_topup == 0) {
                                    var cek = `<a class="dropdown-item p-2 gantistatus" id="${row.id_topup}" data-status="1" href="javascript:void(0)"><i class="bi bi-check-circle-fill mt-4 p-2"></i> Validasi</a>
                                                <a class="dropdown-item p-2 gantistatus" id="${row.id_topup}" data-status="2" href="javascript:void(0)"><i class="bi bi-x mt-4 p-2"></i> Batalkan</a>`;
                                } else {
                                    var cek = "";
                                }
                                $rowData += `
                                        <div class="dropdown">
                                            <button class="btn btn-primary btn-sm dropdown-toggle p-2" type="button" id="dropdownMenuIconButton1" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="bi bi-menu-button-wide"></i>
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton1">
                                                ${cek}
                                            </div>
                                        </div>
                                                    `;

                                return $rowData;
                            },
                            aTargets: [8],
                        },
                        {
                            mRender: function (data, type, row) {
                                var $rowData = "Rp. " + addCommas(row.nominal);
                                return $rowData;
                            },
                            aTargets: [2],
                        },
                        {
                            mRender: function (data, type, row) {
                                var $rowData = "";
                                if (row.status_topup == 1) {
                                    $rowData =
                                        '<span class="badge bg-success p-2"> Berhasil </span>';
                                } else if (row.status_topup == 2) {
                                    $rowData =
                                        '<span class="badge bg-danger p-2"> Gagal </span>';
                                } else {
                                    $rowData =
                                        '<span class="badge bg-warning p-2"> Belum dilakukan verifikasi </span>';
                                }
                                return $rowData;
                            },
                            aTargets: [7],
                        },
                        {
                            mRender: function (data, type, row) {
                                if (row.bukti_pembayaran == null || row.bukti_pembayaran == "") {
                                    var $rowData = ""
                                } else {
                                    var $rowData =
                                        `<img src="` +
                                        row.bukti_pembayaran +
                                        `" class="rounded" style="max-width: 70%; alt="default.jpg">`;
                                }
                                return $rowData;
                            },
                            aTargets: [4],
                        },
                    ],

                    fnRowCallback: function (
                        nRow,
                        aData,
                        iDisplayIndex,
                        iDisplayIndexFull
                    ) {
                        var index = iDisplayIndexFull + 1;
                        $("td:eq(0)", nRow).html(" " + index);
                        return;
                    },

                    fnInitComplete: function () {
                        var that = this;
                        var td;
                        var tr;

                        this.$("td").click(function () {
                            td = this;
                        });
                        this.$("tr").click(function () {
                            tr = this;
                        });

                        $("#listproj_filter input").bind("keyup", function (e) {
                            return this.value;
                        });
                    },
                });
            },
        });
    }

    function gantistatus(id, status) {
        Swal.fire({
            title: "Anda Yakin, mengganti status top up ini?",
            text: "",
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: '<i class="fas fa-check"></i> Ya',
            cancelButtonText: '<i class="fas fa-times"></i> Tidak',
            reverseButtons: true,
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    type: "post",
                    dataType: "json",
                    url: url + "/approve-top-up",
                    data: {
                        id_topup: id,
                        status_topup: status,
                        _token: token,
                    },
                    success: function (data) {
                        if (data.status == 201) {
                            Swal.fire({
                                icon: "success",
                                text: data.message,
                            }).then((results) => {
                                if (results.isConfirmed) {
                                    loaddata();
                                }
                            });
                        } else {
                            Swal.fire({
                                icon: "error",
                                text: result.message,
                            });
                        }
                    },
                });
            }
        });
    }
    function addCommas(nStr) {
        nStr += "";
        x = nStr.split(".");
        x1 = x[0];
        x2 = x.length > 1 ? "." + x[1] : "";
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, "$1" + "," + "$2");
        }
        return x1 + x2;
    }
});
