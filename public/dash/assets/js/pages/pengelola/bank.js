$(function () {
    $("#page-bank").addClass("active");

    // $("#table-users").DataTable({
    //     dom: "Bfrtip",
    //     buttons: ["copy", "csv", "excel", "pdf", "print"],
    // });

    loaddata();

    $(document).on("click", ".edititem", function () {
        var id = $(this).attr("id");
        var bank_name = $(this).data("bank");
        var account_name = $(this).data("name");
        var account_number = $(this).data("number");
        edititem(id, bank_name, account_name, account_number);
    });
    $(document).on("click", ".deleteitem", function () {
        var id = $(this).attr("id");
        deleteitem(id);
    });

    function loaddata() {
        $.ajax({
            type: "get",
            dataType: "json",
            url: url + "/list-transfer-account",
            data: {
                _token: token,
            },
            success: function (result) {
                var dt = $("#table-bank").DataTable({
                    responsive: true,
                    bDestroy: true,
                    processing: false,
                    autoWidth: true,
                    pageLength: 10,
                    lengthChange: true,
                    aaData: result.data,
                    dom: "Bfrtip",
                    buttons: ["copy", "csv", "excel", "pdf", "print"],
                    aoColumns: [
                        { mDataProp: "id_bank_account" },
                        { mDataProp: "bank_name" },
                        { mDataProp: "account_name" },
                        { mDataProp: "account_number" },
                        { mDataProp: "id_bank_account" },
                        // { 'mDataProp': 'role'},
                    ],
                    order: [[0, "ASC"]],
                    aoColumnDefs: [
                        {
                            mRender: function (data, type, row) {
                                var $rowData = "";
                                $rowData +=
                                    `
                                        <div class="dropdown">
                                            <button class="btn btn-primary btn-sm dropdown-toggle p-2" type="button" id="dropdownMenuIconButton1" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="bi bi-menu-button-wide"></i>
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton1">
                                                <a class="dropdown-item p-2 edititem" id="` +
                                    row.id_bank_account +
                                    `" href="javascript:void(0)" data-bank="${row.bank_name}" data-name="${row.account_name}" data-number="${row.account_number}"><i class="bi bi-pen mt-4 p-2"></i> Edit</a>
                                                <a class="dropdown-item p-2 deleteitem" id="` +
                                    row.id_bank_account +
                                    `" href="javascript:void(0)"><i class="bi bi-trash mt-4 p-2"></i> Hapus</a>
                                            </div>
                                        </div>
                                                    `;

                                return $rowData;
                            },
                            aTargets: [4],
                        },
                    ],

                    fnRowCallback: function (
                        nRow,
                        aData,
                        iDisplayIndex,
                        iDisplayIndexFull
                    ) {
                        var index = iDisplayIndexFull + 1;
                        $("td:eq(0)", nRow).html(" " + index);
                        return;
                    },

                    fnInitComplete: function () {
                        var that = this;
                        var td;
                        var tr;

                        this.$("td").click(function () {
                            td = this;
                        });
                        this.$("tr").click(function () {
                            tr = this;
                        });

                        $("#listproj_filter input").bind("keyup", function (e) {
                            return this.value;
                        });
                    },
                });
            },
        });
    }

    $("#formBank").submit(function (e) {
        e.preventDefault();
        var id = $("#id").val();
        if (id) {
            var bank_name = $("#floatName").val();
            var account_name = $("#floatAccname").val();
            var account_number = $("#floatAcc").val();
            $.ajax({
                type: "POST",
                dataType: "JSON",
                data: {
                    id_bank_account: id,
                    bank_name: bank_name,
                    account_name: account_name,
                    account_number: account_number,
                    _token: token,
                },
                url: url + "/edit-transfer-account",
                success: function (result) {
                    if (result.status == 201) {
                        Swal.fire({
                            icon: "success",
                            text: result.message,
                        }).then((results) => {
                            if (results.isConfirmed) {
                                loaddata();
                                $("#btn-reset").trigger("click");
                                $("#closemodal").trigger("click");
                            }
                        });
                    } else {
                        Swal.fire({
                            icon: "error",
                            text: result.message,
                        });
                    }
                },
            });
        } else {
            $.ajax({
                type: "POST",
                dataType: "JSON",
                data: new FormData(this),
                processData: false,
                contentType: false,
                url: url + "/tambah-transfer-account",
                success: function (result) {
                    if (result.status == 201) {
                        Swal.fire({
                            icon: "success",
                            text: result.message,
                        }).then((results) => {
                            if (results.isConfirmed) {
                                loaddata();
                                $("#btn-reset").trigger("click");
                                $("#closemodal").trigger("click");
                            }
                        });
                    } else {
                        Swal.fire({
                            icon: "error",
                            text: result.message,
                        });
                    }
                },
            });
        }
    });

    function edititem(id, bank_name, account_name, account_number) {
        $("#btn-form").trigger("click");
        $("#id").val(id);
        $("#floatName").val(bank_name);
        $("#floatAcc").val(account_number);
        $("#floatAccname").val(account_name);
    }

    function deleteitem(id) {
        Swal.fire({
            title: "Yakin Di Hapus ?",
            text: "Data akan terhapus permanen !",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Ya, Hapus !",
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    type: "POST",
                    dataType: "JSON",
                    data: { id_bank_account: id, _token: token },
                    url: url + "/delete-transfer-account",
                    success: function (result) {
                        if (result.status == 201) {
                            Swal.fire({
                                icon: "success",
                                text: result.message,
                            }).then((results) => {
                                if (results.isConfirmed) {
                                    loaddata();
                                }
                            });
                        } else {
                            Swal.fire({
                                icon: "error",
                                text: result.message,
                            });
                        }
                    },
                });
            }
        });
    }
});
