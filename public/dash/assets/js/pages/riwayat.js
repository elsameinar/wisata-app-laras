$(function () {
    $("#page-riwayat").addClass("active");

    if (role == "Pengelola") {
        loaddata();

        function loaddata() {
            $.ajax({
                type: "post",
                dataType: "json",
                url: url + "/riwayat-pembelian-tiket",
                data: {
                    _token: token,
                },
                success: function (result) {
                    var dt = $("#table-riwayat").DataTable({
                        responsive: true,
                        bDestroy: true,
                        processing: false,
                        autoWidth: true,
                        pageLength: 10,
                        lengthChange: true,
                        aaData: result.data,
                        dom: "Bfrtip",
                        buttons: ["copy", "csv", "excel", "pdf", "print"],
                        aoColumns: [
                            { mDataProp: "id_pembelian" },
                            { mDataProp: "nama" },
                            { mDataProp: "nama_wahana" },
                            { mDataProp: "jumlah_tiket_awal" },
                            { mDataProp: "jumlah_tiket" },
                            { mDataProp: "jumlah_pembayaran" },
                            { mDataProp: "tanggal_pembelian" },
                            { mDataProp: "id_pembelian" },
                            // { 'mDataProp': 'role'},
                        ],
                        order: [[0, "ASC"]],
                        aoColumnDefs: [
                            {
                                mRender: function (data, type, row) {
                                    var $rowData = "";
                                    if (row.status == 0 && row.jumlah_tiket_awal > row.jumlah_tiket ) {
                                        $rowData = `<span class="badge bg-warning p-2"> Sudah Check-in Sebagian </span>`;
                                    }else if (row.status == 0) {
                                        $rowData = `<span class="badge bg-danger p-2"> Belum Check-in </span>`;
                                    } else if (row.status == 1) {
                                        $rowData = `<span class="badge bg-success p-2"> Sudah digunakan </span>`;
                                    }
                                    return $rowData;
                                },
                                aTargets: [7],
                            },
                            // {
                            //     mRender: function (data, type, row) {
                            //         var $rowData =
                            //             `<img src="` +
                            //             row.foto_wahana +
                            //             `" class="img-thumbnail" style="max-width: 100%; alt="default.jpg">`;
                            //         return $rowData;
                            //     },
                            //     aTargets: [1],
                            // },
                            // {
                            //     mRender: function (data, type, row){
                            //       var $rowData = '<img src="'+row.foto+'" style="width: 35px;"></img>';
                            //         return $rowData;
                            //     },
                            //     aTargets: [1]
                            // }
                        ],

                        fnRowCallback: function (
                            nRow,
                            aData,
                            iDisplayIndex,
                            iDisplayIndexFull
                        ) {
                            var index = iDisplayIndexFull + 1;
                            $("td:eq(0)", nRow).html(" " + index);
                            return;
                        },

                        fnInitComplete: function () {
                            var that = this;
                            var td;
                            var tr;

                            this.$("td").click(function () {
                                td = this;
                            });
                            this.$("tr").click(function () {
                                tr = this;
                            });

                            $("#listproj_filter input").bind(
                                "keyup",
                                function (e) {
                                    return this.value;
                                }
                            );
                        },
                    });
                },
            });
        }
    } else if (role == "Wisatawan") {
        $.ajax({
            type: "post",
            dataType: "json",
            url: url + "/riwayat-pembelian-tiket",
            data: {
                id_user: id_user,
                _token: token,
            },
            success: function (result) {
                var res = result.data;
                var html = "";
                if (res.length > 0) {
                    for (var i = 0; i < res.length; i++) {
                        if (res[i].status == 0 && res[i].jumlah_tiket_awal > res[i].jumlah_tiket ) {
                            var cek =
                                '<span class="fw-bold fs-6 text-warning d-block">Sebagian sudah di pakai</span>';
                        } else if (res[i].status == 1) {
                            var cek =
                                '<span class="fw-bold fs-6 text-success d-block">Sudah di pakai</span>';
                        } else {
                            var cek =
                                '<span class="fw-bold fs-6 text-danger d-block">Belum Check-in</span>';
                        }
                        html += `<div class="col-lg-4">
                                <div class="card">
                                    <div class="card-body p-4">
                                        <div class="row">
                                            <div class="col-md-7">
                                                <h5>Pemesanan ${res[i].nama_wahana}</h5>
                                                <h6>${res[i].jumlah_tiket_awal-res[i].jumlah_tiket}/${res[i].jumlah_tiket_awal} Tiket Dipakai</h6>
                                                <h6>Total ${res[i].jumlah_pembayaran}</h6>
                                            </div>
                                            <div class="col-md-5">
                                                ${cek}`;
                        if (res[i].status == 0) {
                            html += `<a href="#" data-url="${res[i].id_pembelian},1,${res[i].nama_wahana}" class="link-primary qr"><small><i class="bx bx-qr"></i>show QR</small></a>`;
                        }
                        html += `
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-footer text-center">
                                    <h6 class="text-muted"> <i class="bi bi-calendar"></i> ${res[i].tanggal_pembelian}</h6>
                                    </div>
                                </div>
                            </div>`;
                    }
                } else {
                    html +=
                        '<div class="col-lg-12"> <h4>Anda Belum Melakukan Transaksi Tiket</h4> </div>';
                }

                $("#riwayat-user").html(html);
            },
        });

        $(document).on("click", ".qr", function () {
            var url = $(this).data("url");
            $("#btn-show-qr").trigger("click");
            generate_qr(url);
        });

        function generate_qr(text) {
            $("#qrcode canvas").remove();
            $("#qrcode").qrcode({
                render: "canvas",
                text: text,
            });
        }
    }
});
