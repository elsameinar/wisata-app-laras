<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\TiketingController;
use App\Http\Controllers\SaldoController;
use App\Http\Controllers\ViewController;
use Illuminate\Http\Request;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('frontend/index');
});
Route::get('register', function () {
    return view('register');
});
Route::get('login', function () {
    return view('login');
});
Route::get('dashboard', [ViewController::class, 'dashboard']);
Route::get('userprofile', [ViewController::class, 'userprofile']);
Route::get('tiket', [ViewController::class, 'tiket']);
Route::get('riwayat', [ViewController::class, 'riwayat']);
Route::get('topup', [ViewController::class, 'topup']);

// pengelola
Route::get('infotiket', [ViewController::class, 'infotiket']);
Route::get('manualtiket', [ViewController::class, 'manualtiket']);
Route::get('reqtopup', [ViewController::class, 'reqtopup']);
Route::get('bank', [ViewController::class, 'bank']);
Route::get('users', [ViewController::class, 'users']);
Route::get('scan', [ViewController::class, 'scan']);


// BACKEND
Route::post('act_login', [AuthController::class, 'actLogin']);
Route::post('act_register', [AuthController::class, 'actRegister']);
Route::post('get_user', [UserController::class, 'getUser']);
Route::post('get_all_user', [UserController::class, 'getAllUser']);
Route::post('change_password', [UserController::class, 'changePassword']);
Route::post('edit_user', [UserController::class, 'editUser']);
Route::post('delete_user', [UserController::class, 'deleteUser']);
Route::get('logout', [UserController::class, 'logout']);
Route::get('csrf_token', function () {
    return csrf_token();
});
// Tiketing
Route::post('tambah-tiket', [TiketingController::class, 'tambahTiket']);
Route::post('beli-tiket', [TiketingController::class, 'beliTiket']);
Route::post('riwayat-pembelian-tiket', [TiketingController::class, 'riwayatPembelianTiket']);
Route::get('list-tiket', [TiketingController::class, 'listTiket']);
Route::post('edit-tiket', [TiketingController::class, 'editTiket']);
Route::post('delete-tiket', [TiketingController::class, 'deleteTiket']);
Route::post('check-in-tiket', [TiketingController::class, 'checkInTicket']);
// Saldo
Route::get('list-nominal-saldo', [SaldoController::class, 'listNominalSaldo']);
Route::post('tambah-nominal-saldo', [SaldoController::class, 'tambahNominalSaldo']);
Route::post('edit-nominal-saldo', [SaldoController::class, 'editNominalSaldo']);
Route::post('delete-nominal-saldo', [SaldoController::class, 'deleteNominalSaldo']);
Route::get('list-transfer-account', [SaldoController::class, 'listTransferAccount']);
Route::post('tambah-transfer-account', [SaldoController::class, 'tambahTransferAccount']);
Route::post('edit-transfer-account', [SaldoController::class, 'editTransferAccount']);
Route::post('delete-transfer-account', [SaldoController::class, 'deleteTransferAccount']);
Route::post('riwayat-top-up', [SaldoController::class, 'riwayatTopUp']);
Route::post('top-up-saldo', [SaldoController::class, 'topUpSaldo']);
Route::post('approve-top-up', [SaldoController::class, 'approveTopUp']);
// QRCode
Route::get('qr_example', function () {
    QrCode::size(500)->format('png')->generate('wisata-app', public_path('images/qrcode.png'));
    return view('qr_example');
});
