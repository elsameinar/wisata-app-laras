
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Registrasi</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="{{ URL('dash/assets/img/favicon.png') }}" rel="icon">
  <link href="{{ URL('dash/assets/img/apple-touch-icon.png') }}" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.gstatic.com" rel="preconnect">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="{{ URL('dash/assets/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ URL('dash/assets/vendor/bootstrap-icons/bootstrap-icons.css') }}" rel="stylesheet">
  <link href="{{ URL('dash/assets/vendor/boxicons/css/boxicons.min.css') }}" rel="stylesheet">
  <link href="{{ URL('dash/assets/vendor/quill/quill.snow.css') }}" rel="stylesheet">
  <link href="{{ URL('dash/assets/vendor/quill/quill.bubble.css') }}" rel="stylesheet">
  <link href="{{ URL('dash/assets/vendor/remixicon/remixicon.css') }}" rel="stylesheet">
  <link href="{{ URL('dash/assets/vendor/simple-datatables/style.css') }}" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="{{ URL('dash/assets/css/style.css') }}" rel="stylesheet">

  <!-- =======================================================
  * Template Name: NiceAdmin - v2.3.1
  * Template URL: https://bootstrapmade.com/nice-admin-bootstrap-admin-html-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <main>
    <div class="container">

      <section class="section register min-vh-100 d-flex flex-column align-items-center justify-content-center py-4">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-lg-4 col-md-6 d-flex flex-column align-items-center justify-content-center">

              <div class="card mb-3">

                <div class="card-body">

                  <div class="pt-4 pb-2">
                    <h5 class="card-title text-center pb-0 fs-4">Buat Akun</h5>
                    <p class="text-center small">Masukan data pribadi anda </p>
                  </div>

                  <form class="row g-3 needs-validation" id="formReg" novalidate>
                    @csrf
                    <div class="col-12">
                      <label for="yourName" class="form-label">Nama Lengkap</label>
                      <input type="text" name="nama" class="form-control" id="yourName" required>
                      <div class="invalid-feedback">Tolong, masukan nama anda !</div>
                    </div>

                    <div class="col-12">
                      <label for="yourTelp" class="form-label">Nomor Telepon</label>
                      <input type="telephone" name="no_telepon" class="form-control" id="yourTelp" required>
                      <div class="invalid-feedback">Tolong, masukan Nomor Telepon anda !</div>
                    </div>

                    <div class="col-12">
                      <label for="yourEmail" class="form-label">Email</label>
                      <input type="email" name="email" class="form-control" id="yourEmail" required>
                      <div class="invalid-feedback">Tolong masukan email yang valid !</div>
                    </div>

                    <div class="col-12">
                      <label for="yourPassword" class="form-label">Password</label>
                      <input type="password" name="password" class="form-control" id="yourPassword" required>
                      <div class="invalid-feedback">Tolong masukan password anda !</div>
                    </div>

                    <div class="col-12">
                      <label for="yourPasswordConf" class="form-label">Confirm Password</label>
                      <input type="password" name="passwordconf" class="form-control" id="yourPasswordConf" required>
                      <div class="invalid-feedback"> Password Confirm tidak sama !</div>
                    </div>

                    <input type="text" value="Wisatawan" name="kategori" hidden>

                    <div class="col-12">
                      <button class="btn btn-primary w-100" id="subm" type="submit">Buat Akun</button>
                    </div>

                    <div class="col-12">
                      <p class="small mb-0">Sudah Punya akun ? <a href="{{ URL('login') }}">Log in</a></p>
                    </div>
                  </form>

                </div>
              </div>
            </div>
          </div>
        </div>

      </section>

    </div>
  </main><!-- End #main -->

  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="{{ URL('dash/assets/vendor/apexcharts/apexcharts.min.js') }}"></script>
  <script src="{{ URL('dash/assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ URL('dash/assets/js/jquery-3.6.0.min.js') }}"></script>
  <script src="{{ URL('dash/assets/js/sweetalert2.all.min.js') }}"></script>
  <script src="{{ URL('dash/assets/js/sweetalert2.all.min.js') }}"></script>
  <script src="{{ URL('dash/assets/vendor/chart.js/chart.min.js') }}"></script>
  <script src="{{ URL('dash/assets/vendor/echarts/echarts.min.js') }}"></script>
  <script src="{{ URL('dash/assets/vendor/quill/quill.min.js') }}"></script>
  <script src="{{ URL('dash/assets/vendor/simple-datatables/simple-datatables.js') }}"></script>
  <script src="{{ URL('dash/assets/vendor/tinymce/tinymce.min.js') }}"></script>
  <script src="{{ URL('dash/assets/vendor/php-email-form/validate.js') }}"></script>

  <!-- Template Main JS File -->
  <script src="{{ URL('dash/assets/js/main.js') }}"></script>

  <script>
    $(function(){
        $('#subm').attr('disabled','disabled');
        $('#yourPasswordConf').keyup(function(){
            var pass = $('#yourPassword').val();
            var conf = $('#yourPasswordConf').val();
            if (conf === pass) {
                $('#subm').removeAttr('disabled');
                $('#yourPasswordConf').removeClass('border border-danger');
                $('#yourPasswordConf').addClass('border border-success');
            }else{
                $('#subm').attr('disabled','disabled');
                $('#yourPasswordConf').removeClass('border border-success');
                $('#yourPasswordConf').addClass('border border-danger');
            }
        })
        var url = `{{ URL('act_register') }}`;
        var log = `{{ URL('login') }}`;

        $('#formReg').submit(function(e){
            e.preventDefault();
            $.ajax({
                type:'POST',
                dataType:'JSON',
                data : new FormData(this),
                processData: false,
                contentType: false,
                url : url,
                success:function(result){
                  if (result.status == 201) {
                    Swal.fire({
                      icon : 'success',
                      text: result.message
                    }).then((res)=> {
                      if (res.isConfirmed) {
                        window.location.href = log
                      }
                    })
                  }
                }
            });
        })
    })
  </script>

</body>

</html>