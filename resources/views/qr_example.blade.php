<!DOCTYPE html>
<html>
<head>
</head>
<body>
    
<div class="visible-print text-center">
    <h1>Laravel 8 - QR Code Generator Example</h1>
     
    {!! QrCode::size(250)->generate('Wisata-App'); !!}
     
    <p>Wisata-App</p>
</div>
    
</body>
</html>