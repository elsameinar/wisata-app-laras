@extends('dash/static/layout');
@section('content')
<main id="main" class="main">

    <div class="pagetitle">
      <h1>Scanning Tiket</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{ URL('dashboard') }}">Dashboard</a></li>
          <li class="breadcrumb-item active">Scanning Tiket</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->
    <section class="section">
      <div class="row">
        <div class="col-lg-6 offset-lg-3">
          <div class="card">
            <div class="card-header">
              <h5 class="card-title">Scanning Tiket</h5>
            </div>
            <div class="card-body p-3">
                <div style="width: 100%" id="reader"></div>
            </div>
          </div>
        </div>
      </div>
    </section>

  </main><!-- End #main -->
@endsection
@section('blockjs')
  <script>
    var url = "{{ URL('') }}";  
    var token = "{{ csrf_token() }}";
  </script>
  <script src="{{ URL('dash/assets/js/pages/pengelola/scan.js') }}"></script>
@endsection