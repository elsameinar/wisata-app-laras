@extends('dash/static/layout');
@section('content')
<main id="main" class="main">

    <div class="pagetitle">
      <h1>Tiket</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{ URL('dashboard') }}">Dashboard</a></li>
          <li class="breadcrumb-item active">Info Tiket</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->
    <section class="section">
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header">
              <button type="button" class="btn btn-primary btn-sm" id="btn-form" data-bs-toggle="modal" data-bs-target="#staticBackdrop">
                <i class="bi bi-plus"></i> Tambah Data
              </button>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                  <table class="table table-stripped" id="table-tiket">
                    <thead>
                        <th>No</th>
                        <th>Foto Wahana</th>
                        <th>Info Wahana</th>
                        <th>Harga Tiket</th>
                        <th>Nama Wahana</th>
                        <th>Jenis Tiket</th>
                        <th>Action</th>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
                </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Modal -->
    <div class="modal fade modal-lg" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="staticBackdropLabel">Form Info Tiket</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" id="closemodal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
            <!-- Floating Labels Form -->
              <form class="row g-3" id="formTiket">
                @csrf
                <div class="col-md-12">
                  <div class="form-floating">
                    <input type="text" id="id" name="id_wahana_fasilitas" hidden>
                    <input type="file" class="form-control" id="floatingImg" name="foto_wahana" placeholder="Foto Wahana">
                    <label for="floatingImg">Foto Wahana</label>
                  </div>
                </div>
                <div class="col-md-12">
                    <textarea name="info_wahana" id="floatingInfo" cols="30" rows="10" class="form-control"></textarea>
                </div>
                <div class="col-md-12">
                  <div class="form-floating">
                    <input type="number" min="0" value="0" name="harga_tiket" id="floatHarga" class="form-control" required>
                    <label for="floatHarga">Harga Tiket</label>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-floating">
                    <input type="text" name="nama_wahana" id="floatNama" class="form-control" required>
                    <label for="floatNama">Nama Wahana</label>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-floating">
                    <select name="jenis_tiket" id="floatingJenis" class="form-control" required>
                        <option value="">PILIH</option>
                        <option value="masuk">Tiket Masuk</option>
                        <option value="wahana">Tiket Wahana</option>
                        <option value="wahana">Tiket saung</option>
                        <option value="wahana">Fasilitas lainnya</option>
                    </select>
                    <label for="floatingJenis">Jenis Tiket</label>
                  </div>
                </div>
                <div class="text-end">
                  <button type="submit" class="btn btn-primary">Tambah Tiket</button>
                  <button type="reset" hidden class="btn btn-secondary reset">Reset</button>
                </div>
              </form><!-- End floating Labels Form -->
          </div>
        </div>
      </div>
    </div>

  </main><!-- End #main -->
@endsection
@section('blockjs')
  <script>
    var url = "{{ URL('') }}";
    var token = "{{ csrf_token() }}";
  </script>
  <script src="{{ URL('dash/assets/js/pages/pengelola/infotiket.js') }}"></script>
@endsection