@extends('dash/static/layout');
@section('content')
<main id="main" class="main">

    <div class="pagetitle">
      <h1>Pembelian Tiket Manual</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{ URL('dashboard') }}">Dashboard</a></li>
          <li class="breadcrumb-item active">Pembelian Tiket Manual</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->
    <section class="section">
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body mt-4">

              <!-- Floating Labels Form -->
              <form class="row g-3" id="formTiket">
                @csrf
                <input type="number" name="status" value="1" hidden>
                <input type="number" name="jumlah_tiket_awal" min="1" id="jumlah_tiket_awal" hidden>
                <div class="col-md-12">
                  <div class="form-floating">
                    <select name="id_tiket" id="floatTipe" name="id_tiket" class="form-control" required>
                        <option value="">PILIH</option>
                    </select>
                    <label for="floatTipe">Tipe Tiket</label>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-floating">
                    <input type="text" id="floatHrg" value="0" readonly class="form-control" readonly required>
                    <label for="floatHrg">Harga Tiket</label>
                  </div>
                </div>
                <div class="col-md-12">
                    <div class="form-floating">
                        <input type="number" name="jumlah_tiket" min="1" id="floatJmlh" value="1" class="form-control" required>
                        <label for="floatJmlh">Jumlah Tiket</label>
                    </div>
                </div>
                <div class="col-md-12">
                  <div class="form-floating">
                    <input type="text" id="floatPm" value="0" readonly class="form-control" autocomplete="off" required>
                    <input type="number" id="jmlh" name="jumlah_pembayaran" value="0" hidden class="form-control" required>
                    <label for="floatPm">Jumlah Pembayaran</label>
                  </div>
                </div>
                <div class="text-end">
                  <button type="submit" class="btn btn-primary">Beli Sekarang</button>
                  <button type="reset" id="btn-reset" hidden class="btn btn-primary">reset</button>
                  {{-- <button type="reset" class="btn btn-secondary">Reset</button> --}}
                </div>
              </form><!-- End floating Labels Form -->

            </div>
          </div>

        </div>
      </div>
    </section>

  </main><!-- End #main -->
@endsection
@section('blockjs')
  <script>
    var url = "{{ URL('') }}";
  </script>
    <script src="{{ URL('dash/assets/js/pages/pengelola/manualtiket.js') }}"></script>
@endsection