@extends('dash/static/layout');
@section('content')
<main id="main" class="main">

    <div class="pagetitle">
      <h1>Verifikasi Saldo</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{ URL('dashboard') }}">Dashboard</a></li>
          <li class="breadcrumb-item active">Verifikasi Saldo</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->
    <section class="section">
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body p-3">
                <div class="table-responsive">
                    <table class="table table-stripped" id="table-reqtopup">
                        <thead>
                            <th>No</th>
                            <th>Nama User</th>
                            <th>Nominal</th>
                            <th>Metode Pembayaran</th>
                            <th>Bukti Pembayaran</th>
                            <th>Tanggal Isi Saldo</th>
                            <th>Tanggal Konfirmasi</th>
                            <th>Status</th>
                            <th>Action</th>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
          </div>

        </div>
      </div>
    </section>

  </main><!-- End #main -->
@endsection
@section('blockjs')
  <script>
    var url = "{{ URL('') }}";  
    var token = "{{ csrf_token() }}";
  </script>
    <script src="{{ URL('dash/assets/js/pages/pengelola/reqtopup.js') }}"></script>
@endsection