@extends('dash/static/layout');
@section('content')
<main id="main" class="main">

    <div class="pagetitle">
      <h1>List Users</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{ URL('dashboard') }}">Dashboard</a></li>
          <li class="breadcrumb-item active">List Users</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->
    <section class="section">
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header">
              <button type="button" class="btn btn-primary btn-sm" data-bs-toggle="modal" data-bs-target="#staticBackdrop">
                <i class="bi bi-plus"></i> Tambah Data
              </button>
            </div>
            <div class="card-body p-3">
                <div class="table-responsive">
                    <table class="table table-stripped" id="table-users">
                        <thead>
                            <th>No</th>
                            <th>Email</th>
                            <th>Kategori</th>
                            <th>Nama</th>
                            <th>No Telepon</th>
                            <th>Saldo</th>
                            <th>Action</th>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
          </div>

        </div>
      </div>

          <!-- Modal -->
    <div class="modal fade modal-lg" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="staticBackdropLabel">Form Tambah Users</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" id="closemodal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
            <!-- Floating Labels Form -->
              <form class="row g-3" id="formUsers">
                @csrf
                <div class="col-md-12">
                  <div class="form-floating">
                    <input type="email" class="form-control" id="floatEmail" name="email" placeholder="Email" required>
                    <label for="floatEmail">Emaill</label>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-floating">
                    <input type="text" value="Pengelola" name="kategori" id="floatKat" class="form-control" required>
                    <label for="floatKat">Kategori</label>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-floating">
                    <input type="text" name="nama" id="floatNama" class="form-control" placeholder="Nama" required>
                    <label for="floatNama">Nama</label>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-floating">
                    <input type="password" name="password" id="floatPass" class="form-control" placeholder="Password" required>
                    <label for="floatPass">Password</label>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-floating">
                    <input type="tel" name="no_telepon" id="floatTelp" class="form-control" placeholder="08123123123" required>
                    <label for="floatTelp">No Telepon</label>
                  </div>
                </div>
                <div class="text-end">
                  <button type="submit" class="btn btn-primary">Tambah Users</button>
                  <button type="reset" id="btn-reset" hidden class="btn btn-primary">reset</button>
                  {{-- <button type="reset" class="btn btn-secondary">Reset</button> --}}
                </div>
              </form><!-- End floating Labels Form -->
          </div>
        </div>
      </div>
    </div>
    </section>

  </main><!-- End #main -->
@endsection
@section('blockjs')
  <script>
    var url = "{{ URL('') }}";  
    var token = "{{ csrf_token() }}";
  </script>
  <script src="{{ URL('dash/assets/js/pages/pengelola/users.js') }}"></script>
@endsection