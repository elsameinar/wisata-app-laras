@extends('dash/static/layout');
@section('content')
<main id="main" class="main">

    <div class="pagetitle">
      <h1>List Bank Account</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{ URL('dashboard') }}">Dashboard</a></li>
          <li class="breadcrumb-item active">List Bank Account</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->
    <section class="section">
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header">
              <button type="button" class="btn btn-primary btn-sm" id="btn-form" data-bs-toggle="modal" data-bs-target="#staticBackdrop">
                <i class="bi bi-plus"></i> Tambah Data
              </button>
            </div>
            <div class="card-body p-3">
                <div class="table-responsive">
                    <table class="table table-stripped" id="table-bank">
                        <thead>
                            <th>No</th>
                            <th>Nama Bank</th>
                            <th>Nomor Rekening</th>
                            <th>Atas Nama</th>
                            <th>Action</th>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
          </div>

        </div>
      </div>

          <!-- Modal -->
    <div class="modal fade modal-lg" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="staticBackdropLabel">Form Bank Account</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" id="closemodal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
            <!-- Floating Labels Form -->
              <form class="row g-3" id="formBank">
                @csrf
                <div class="col-md-12">
                  <div class="form-floating">
                    <input type="text" id="id" hidden>
                    <input type="text" class="form-control" id="floatName" name="bank_name" placeholder="Nama Bank" required>
                    <label for="floatName">Nama Bank</label>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-floating">
                    <input type="text" name="account_number" id="floatAcc" class="form-control" placeholder="Nomor Rekening" required>
                    <label for="floatAcc">Nomor Rekening</label>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-floating">
                    <input type="text" name="account_name" id="floatAccname" class="form-control" placeholder="Atas Nama" required>
                    <label for="floatAccname">Atas Nama</label>
                  </div>
                </div>
                <div class="text-end">
                  <button type="submit" class="btn btn-primary">Simpan</button>
                  <button type="reset" id="btn-reset" hidden class="btn btn-primary">reset</button>
                  {{-- <button type="reset" class="btn btn-secondary">Reset</button> --}}
                </div>
              </form><!-- End floating Labels Form -->
          </div>
        </div>
      </div>
    </div>
    </section>

  </main><!-- End #main -->
@endsection
@section('blockjs')
  <script>
    var url = "{{ URL('') }}";  
    var token = "{{ csrf_token() }}";
  </script>
  <script src="{{ URL('dash/assets/js/pages/pengelola/bank.js') }}"></script>
@endsection