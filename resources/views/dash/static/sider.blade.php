<!-- ======= Sidebar ======= -->
  <aside id="sidebar" class="sidebar">

    <ul class="sidebar-nav" id="sidebar-nav">
      @if (Session::get('kategori') == 'Wisatawan')
        <li class="nav-item">
          <a class="nav-link " id="page-dashboard" href="{{ URL('dashboard') }}">
            <i class="bi bi-grid"></i>
            <span>Dashboard</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link " id="page-topup" href="{{ URL('topup') }}">
            <i class="bi bi-credit-card"></i>
            <span>Topup Saldo</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link " id="page-tiket" href="{{ URL('tiket') }}">
            <i class="bi bi-ticket"></i>
            <span>Pesan Tiket</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link " id="page-riwayat" href="{{ URL('riwayat') }}">
            <i class="bi bi-ticket"></i>
            <span>Tiketmu</span>
          </a>
        </li>
        <!-- <li class="nav-item">
          <a class="nav-link " id="page-tiket" href="{{ URL('tiket') }}">
            <i class="bi bi-credit-card"></i>
            <span>Saldomu</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link " id="page-dashboard" href="{{ URL('dashboard') }}">
            <i class="bi bi-map"></i>
            <span>peta kawasan</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link " id="page-riwayat" href="{{ URL('riwayat') }}">
            <i class="bi bi-clock-history"></i>
            <span>Riwayat Transaksi</span>
          </a>
        </li> -->
        
      @else
        <li class="nav-item">
          <a class="nav-link " id="page-dashboard" href="{{ URL('dashboard') }}">
            <i class="bi bi-grid"></i>
            <span>Dashboard</span>
          </a>
        </li>
        
        <li class="nav-item">
          <a class="nav-link collapsed" data-bs-target="#icons-nav" id="tiket-collapse" data-bs-toggle="collapse" href="#">
            <i class="bi bi-ticket"></i><span>Tiket</span><i class="bi bi-chevron-down ms-auto"></i>
          </a>
          <ul id="icons-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
            <li>
              <a href="{{ URL('infotiket') }}" id="page-infotiket">
                <i class="bi bi-circle"></i><span>Info Tiket</span>
              </a>
            </li>
            <li>
              <a href="{{ URL('manualtiket') }}" id="page-manualtiket">
                <i class="bi bi-circle"></i><span>Pembelian Tiket Manual</span>
              </a>
            </li>
          </ul>
        </li><!-- End Icons Nav -->
        <li class="nav-item">
          <a class="nav-link " id="page-scan" href="{{ URL('scan') }}">
            <i class="bi bi-qr-code-scan"></i>
            <span>Scanning Tiket</span>
          </a>
        </li>
        
        <li class="nav-item">
          <a class="nav-link " id="page-reqtopup" href="{{ URL('reqtopup') }}">
            <i class="bi bi-credit-card"></i>
            <span>Verifikasi Saldo</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link " id="page-bank" href="{{ URL('bank') }}">
            <i class="bi bi-bank"></i>
            <span>Bank Account</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link " id="page-users" href="{{ URL('users') }}">
            <i class="bi bi-people"></i>
            <span>Users</span>
          </a>
        </li>
        <!-- <li class="nav-item">
          <a class="nav-link " id="page-riwayat" href="{{ URL('riwayat') }}">
            <i class="bi bi-folder"></i>
            <span>Laporan</span>
          </a>
        </li> -->
        <li class="nav-item">
          <a class="nav-link " id="page-riwayat" href="{{ URL('riwayat') }}">
            <i class="bi bi-clock-history"></i>
            <span>Riwayat Transaksi</span>
          </a>
        </li>
      @endif

      {{-- <li class="nav-item">
        <a class="nav-link " href="index.html">
          <i class="bi bi-grid"></i>
          <span>Dashboard</span>
        </a>
      </li> --}}

    </ul>

  </aside><!-- End Sidebar-->