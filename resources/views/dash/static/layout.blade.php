<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>User Pages</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="{{ URL('dash/assets/img/favicon.png') }}" rel="icon">
  <link href="{{ URL('dash/assets/img/apple-touch-icon.png') }}" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.gstatic.com" rel="preconnect">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="{{ URL('dash/assets/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ URL('dash/assets/vendor/bootstrap-icons/bootstrap-icons.css') }}" rel="stylesheet">
  <link href="{{ URL('dash/assets/vendor/boxicons/css/boxicons.min.css') }}" rel="stylesheet">
  <link href="{{ URL('dash/assets/css/summernote-bs4.min.css') }}" rel="stylesheet">
  <link href="{{ URL('dash/assets/vendor/quill/quill.snow.css') }}" rel="stylesheet">
  <link href="{{ URL('dash/assets/vendor/quill/quill.bubble.css') }}" rel="stylesheet">
  <link href="{{ URL('dash/assets/vendor/remixicon/remixicon.css') }}" rel="stylesheet">
  <link href="{{ URL('dash/assets/vendor/simple-datatables/style.css') }}" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="{{ URL('dash/assets/vendor/DataTables/datatables.min.css') }}"/>
  <script type="text/javascript" src="{{ URL('dash/assets/vendor/DataTables/Buttons-2.2.3/css/buttons.bootstrap5.min.css') }}"></script>

  <!-- Template Main CSS File -->
  <link href="{{ URL('dash/assets/css/style.css') }}" rel="stylesheet">

  <!-- =======================================================
  * Template Name: NiceAdmin - v2.3.1
  * Template URL: https://bootstrapmade.com/nice-admin-bootstrap-admin-html-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>
    @include('dash/static/header')
    @include('dash/static/sider')
    @yield('content')
  <!-- ======= Footer ======= -->
  {{-- <footer id="footer" class="footer">
    <div class="copyright">
      &copy; Copyright <strong><span>NiceAdmin</span></strong>. All Rights Reserved
    </div>
    <div class="credits">   
      <!-- All the links in the footer should remain intact. -->
      <!-- You can delete the links only if you purchased the pro version. -->
      <!-- Licensing information: https://bootstrapmade.com/license/ -->
      <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/nice-admin-bootstrap-admin-html-template/ -->
      Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
    </div>
  </footer><!-- End Footer --> --}}

  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="{{ URL('dash/assets/vendor/apexcharts/apexcharts.min.js') }}"></script>
  <script src="{{ URL('dash/assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ URL('dash/assets/js/jquery-3.6.0.min.js') }}"></script>
  <script src="{{ URL('dash/assets/js/sweetalert2.all.min.js') }}"></script>
  <script src="{{ URL('dash/assets/js/html5-qrcode.min.js') }}"></script>
  <script src="{{ URL('dash/assets/js/summernote-bs4.min.js') }}"></script>
  <script src="{{ URL('dash/assets/js/moment.min.js') }}"></script>
  <script type="text/javascript" src="{{ URL('dash/assets/vendor/DataTables/datatables.min.js') }}"></script>
  <script type="text/javascript" src="{{ URL('dash/assets/vendor/DataTables/Buttons-2.2.3/js/buttons.bootstrap5.min.js') }}"></script>
  <script type="text/javascript" src="{{ URL('dash/assets/vendor/DataTables/pdfmake-0.1.36/pdfmake.js') }}"></script>
  <script type="text/javascript" src="{{ URL('dash/assets/vendor/DataTables/pdfmake-0.1.36/vfs_fonts.js') }}"></script>
  <script src="{{ URL('dash/assets/vendor/chart.js/chart.min.js') }}"></script>
  <script src="{{ URL('dash/assets/vendor/echarts/echarts.min.js') }}"></script>
  <script src="{{ URL('dash/assets/vendor/quill/quill.min.js') }}"></script>
  <script src="{{ URL('dash/assets/vendor/simple-datatables/simple-datatables.js') }}"></script>
  <script src="{{ URL('dash/assets/vendor/tinymce/tinymce.min.js') }}"></script>
  <script src="{{ URL('dash/assets/vendor/php-email-form/validate.js') }}"></script>

  <!-- Template Main JS File -->
  <script src="{{ URL('dash/assets/js/main.js') }}"></script>
  @yield('blockjs')

</body>

</html>