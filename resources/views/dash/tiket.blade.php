@extends('dash/static/layout');
@section('content')
<main id="main" class="main">

    <div class="pagetitle">
      <h1>Pesan Tiket</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{ URL('dashboard') }}">Dashboard</a></li>
          <li class="breadcrumb-item active">Pesan Tiket</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->
    <button type="button" class="btn btn-primary btn-sm" id="btn-tiket" data-bs-toggle="modal" hidden data-bs-target="#staticBackdrop">
                <i class="bi bi-plus"></i>
              </button>
    <section class="section">
      <div class="row" id="list-tiket">
      </div>
                <!-- Modal -->
    <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="staticBackdropLabel">Form Beli Tiket</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" id="closemodal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
            <div class="card">
              <img id="foto_wahana" class="card-img-top" alt="...">
              <div class="card-body">
                <h5 class="card-title" id="namaTiket"></h5>
                <form class="row g-3" id="formTiket">
                @csrf
                <input type="number" name="id_user" id="id_user" value="{{ Session::get('id_user') }}" hidden>
                <input type="number" name="id_tiket" id="id_tiket" value="1" hidden>
                <input type="number" name="jumlah_tiket_awal" min="1" id="jumlah_tiket_awal" hidden>
                <div class="col-md-12">
                  <div class="form-floating">
                    <input type="text" id="floatHrg" value="0" readonly class="form-control" readonly required>
                    <label for="floatHrg">Harga Tiket</label>
                  </div>
                </div>
                <div class="col-md-12">
                    <div class="form-floating">
                        <input type="number" name="jumlah_tiket" min="1" id="floatJmlh" value="1" class="form-control" required>
                        <label for="floatJmlh">Jumlah Tiket</label>
                    </div>
                </div>
                <div class="col-md-12">
                  <div class="form-floating">
                    <input type="text" id="floatPm" value="0" readonly class="form-control" required>
                    <input type="number" id="jmlh" name="jumlah_pembayaran" hidden class="form-control" required>
                    <label for="floatPm">Jumlah Pembayaran</label>
                  </div>
                </div>
                <div class="text-end">
                  <button type="submit" class="btn btn-primary">Beli Sekarang</button>
                  <button type="reset" id="btn-reset" hidden class="btn btn-primary">reset</button>
                  {{-- <button type="reset" class="btn btn-secondary">Reset</button> --}}
                </div>
              </form><!-- End floating Labels Form -->
              </div>
            </div>
              
          </div>
        </div>
      </div>
    </div>
    </section>

  </main><!-- End #main -->
@endsection
@section('blockjs')
<script>
    var url = "{{ URL('') }}";  
    var token = "{{ csrf_token() }}";
  </script>
    <script src="{{ URL('dash/assets/js/pages/tiket.js') }}"></script>
@endsection