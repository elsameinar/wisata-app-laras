@extends('dash/static/layout');
@section('content')
<main id="main" class="main">

    <div class="pagetitle">
      <h1>Riwayat Transaksi Tiket</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{ URL('dashboard') }}">Dashboard</a></li>
          <li class="breadcrumb-item active">Riwayat Transaksi Tiket</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->
    <button type="button" class="btn btn-primary btn-sm" data-bs-toggle="modal" id="btn-show-qr" data-bs-target="#staticBackdrop" hidden>
                <i class="bi bi-plus"></i> Tambah Data
              </button>
    <section class="section">
      @if (Session::get('kategori') == "Wisatawan")
      <div class="row" id="riwayat-user">
      </div>
                <!-- Modal -->
    <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="staticBackdropLabel">Qr Code Tiket</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" id="closemodal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
            <div id="qrcode" class="text-center"></div>
          </div>
        </div>
      </div>
    </div>
      @else 
      <div class="row">
        <div class="col-lg-12">
          <div class="card g-3">
            <div class="card-body">
              <div class="table-responsive mt-4">
                <table class="table table-stripped table-bordered p-3" id="table-riwayat">
                  <thead>
                    <th>No</th>
                    <th>Nama User</th>
                    <th>Tipe Tiket</th>
                    <th>Jumlah Tiket</th>
                    <th>Jumlah Tiket Tersisa</th>
                    <th>Jumlah Pembayaran</th>
                    <th>Tanggal Pembelian</th>
                    <th>Status</th>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      @endif
    </section>

  </main><!-- End #main -->
@endsection
@section('blockjs')
  <script>
    var url = "{{ URL('') }}";  
    var token = "{{ csrf_token() }}";
    var role = "{{Session::get('kategori')}}";
    var id_user = "{{ Session::get('id_user') }}";
  </script>
  @if (Session::get('kategori') == "Pengelola")
    <script src="{{ URL('dash/assets/js/pages/riwayat.js') }}"></script>  
    @else
    <script src="{{ URL('dash/assets/js/jquery-qr-code.js') }}"></script>  
    <script src="{{ URL('dash/assets/js/pages/riwayat.js') }}"></script>  
    @endif
@endsection