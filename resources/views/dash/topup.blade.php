@extends('dash/static/layout');
@section('content')
<main id="main" class="main">

    <div class="pagetitle">
      <h1>Topup Saldo</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{ URL('dashboard') }}">Dashboard</a></li>
          <li class="breadcrumb-item active">Topup</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->
    <section class="section">
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body p-3">

              <!-- Floating Labels Form -->
              <form class="row g-3" id="formTopup">
                @csrf
                <div class="col-md-12">
                  <input type="text" name="id_user" value="{{ Session::get('id_user') }}" hidden>
                  <div class="form-floating">
                    <select name="metode_pembayaran" id="floatingRek" class="form-control">
                        <option value="">PILIH</option>
                    </select>
                    <label for="floatingRek">Pilih Rekening</label>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-floating">
                    <input type="text" id="floatJum" class="form-control"  autocomplete="off">
                    <input type="number" id="floatNom" name="nominal" value="0" hidden required>
                    <input type="text" name="status_topup" value="0" hidden>
                    <label for="floatingJum">Nominal Topup</label>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-floating formFloatingImg">
                    <input type="file" class="form-control" id="floatingImg" name="bukti_pembayaran" placeholder="Bukti Pembayaran">
                    <label for="floatingImg">Bukti Pembayaran</label>
                  </div>
                </div>
                <div class="text-end">
                  <button type="submit" class="btn btn-primary">Topup</button>
                  <button type="reset" hidden class="btn btn-secondary reset">Reset</button>
                </div>
              </form><!-- End floating Labels Form -->

            </div>
          </div>

        </div>
        <div class="col-lg-12">
            <h4 class="card-title">Riwayat Topup</h4>
            <div class="row" id="riwayat-topup">
                
            </div>
        </div>
      </div>
    </section>

  </main><!-- End #main -->
@endsection
@section('blockjs')
<script>
    var url = "{{ URL('') }}";  
    var token = "{{ csrf_token() }}";
    var id_user = "{{ Session::get('id_user') }}"
  </script>
    <script src="{{ URL('dash/assets/js/pages/topup.js') }}"></script>
@endsection